# Milvus Clustering

install necessary libraries from 
 *"requirements.txt"* and *"milvus_213_pre_requirements.odt"*

**params**
change the parameters in params.yaml file
To add golden signatures (initially) change the "add_signatures : 1" and "gen_clusters : 0"
this will create a table and add all the golden signatures

To search the test embeddings against golden signatures change "gen_clusters : 1" and "add_signatures : 0"

**run**

```
python3 milvus_clustering.py
```

**outputs**

CSV folder with top ten predictions and its rescpective ground truths and latency




