from glob import glob
import os
import numpy as np
import json
from tensorflow.keras.models import Model, model_from_json
import clustering as cluster
import add_golden_signatures as gs
import yaml

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

stream = open("params.yaml")
print(stream)
gen = yaml.load(stream, Loader=yaml.FullLoader)
print(gen["params"])
params = gen["params"]

test = params["test"]
golden_signatures = params["golden_signatures"]

gol_sig_json = params["gol_sig_json"]
test_json = params["test_json"]

table_name = params["table_name"]

csv_path = params["csv_path"]

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



gol_sig_embeddings = []
gol_sig_file_names = []

test_embeddings = []
test_file_names = []

gol_sig_folders = glob(golden_signatures+'/*')
print('total golden signatures files:',len(gol_sig_folders))

test_folders = glob(test+'*')
print('total test files:',len(gol_sig_folders))


for gs_fol in gol_sig_folders[:]:
    each_fol_name = gs_fol.split('/')[-1]
    emb = np.load(gs_fol)
    emb = np.squeeze(emb)  
    emb /= np.linalg.norm(emb, axis=0, keepdims=True)
    gol_sig_embeddings.append(emb)
    gol_sig_file_names.append(each_fol_name)
for te_fol in gol_sig_folders[:]:
    each_fol_name = te_fol.split('/')[-1]
    emb = np.load(te_fol)
    emb = np.squeeze(emb)  
    emb /= np.linalg.norm(emb, axis=0, keepdims=True)
    test_embeddings.append(emb)
    test_file_names.append(each_fol_name)




gol_sig_json_object = json.dumps((gol_sig_file_names, gol_sig_embeddings),cls=NpEncoder)
te_json_object = json.dumps((test_file_names, test_embeddings),cls=NpEncoder)

with open(gol_sig_json, "w") as outfile:
    outfile.write(gol_sig_json_object)

with open(test_json, "w") as outfile:
    outfile.write(te_json_object)


# #Running docker image
# print('running docker image')
# os.system('sudo docker-compose up -d')

gen_clusters = params["gen_clusters"]
add_signatures = params["add_signatures"]


if add_signatures == 1:
    print('Adding Golden Signatures ...')
    gs.add_sig(
        embedding_file_path = test_json,
        graph_threshold = 0,
        curation_threshold = 0.95,
        table_name = table_name
        ) 
else: 
    print("Added Golden Signatures")
if gen_clusters == 1:
    print('Generating Clusters ...')
    cluster.generate_clusters(
        embedding_file_path = test_json,
        graph_threshold = 0,
        curation_threshold = 0.95,
        table_name = table_name,
        csv_path = csv_path
        ) 
else: 
    print("Not Adding Golden Signatures")


