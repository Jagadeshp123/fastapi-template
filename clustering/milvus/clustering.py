import networkx as nx
import time
import csv
import json
# import pickle5 as pickle
from datetime import datetime
# from memory_profiler import profile
from tqdm import tqdm 
import string
import random
import os


from pymilvus import (
    connections,
    utility,
    FieldSchema, CollectionSchema, DataType,
    Collection,
)

class MilvusClient():
    def __init__(self):
        self.connections = connections

    def connect(self):
        self.connections.connect("default", host="localhost", port="19530")

    def disconnect(self):
        self.connections.disconnect()

    def has_table(self, table_name):
        status, ok = self.client.has_collection(table_name)
        return ok

    def create_table(self, table_name, dimension):
        if utility.has_collection(table_name):
            print('tabel name {} exist!!!'.format(table_name))
            # print('deleting the existing table')
            # utility.drop_collection(table_name)
            # print('creating new table..!')
            # fields = [
            #     FieldSchema(name="pk", dtype=DataType.VARCHAR, max_length=200, is_primary=True, auto_id=False),
            #     FieldSchema(name="embeddings", dtype=DataType.FLOAT_VECTOR, dim=dimension)
            # ]

            # schema = CollectionSchema(fields)
            # table_name = Collection(table_name, schema, consistency_level='Strong')
        else:
            print('table does not exist..,creating new table')
            fields = [
                FieldSchema(name="pk", dtype=DataType.VARCHAR, max_length=200, is_primary=True, auto_id=False),
                FieldSchema(name="embeddings", dtype=DataType.FLOAT_VECTOR, dim=dimension)
            ]

            schema = CollectionSchema(fields)
            table_name = Collection(table_name, schema, consistency_level='Strong')

    def describe_table(self, table_name):
        if utility.has_collection(table_name):     
            collection = Collection(table_name)
            print(collection.schema)                # Return the schema.CollectionSchema of the collection.
            print(collection.description)           # Return the description of the collection.
            print(collection.name)                  # Return the name of the collection.
            print(collection.is_empty)              # Return the boolean value that indicates if the collection is empty.
            print(collection.num_entities)          # Return the number of entities in the collection.
            print(collection.primary_field)         # Return the schema.FieldSchema of the primary key field.
            print(collection.partitions)            # Return the list[Partition] object.
            print(collection.indexes)               # Return the list[Index] object.
                    

    def add_vectors(self, table_name, primary_keys, vectors):
        vector_size = 4 * len(vectors[0])
        print('size of a vector: {} bytes'.format(vector_size))
        insert_buf_size = pow(2, 30)
        print('maximum size of adding at a time: {} bytes'.format(insert_buf_size))
        batch_size = min(len(vectors), insert_buf_size // vector_size)
        print('adding {}/{} at a time ...'.format(batch_size, len(vectors)))
        ids = []
        for start in range(0, len(vectors), batch_size):
            batch_vectors = vectors[start:start + batch_size]
            primary_keys_batch = primary_keys[start:start + batch_size] 
            insert_result = table_name.insert([primary_keys_batch,batch_vectors])
            inserted_keys = insert_result.primary_keys
            ids.extend(inserted_keys)
            time.sleep(2)
        return ids

    def remove_vectors(self, table_name, ids):
        status = self.client.delete_by_id(collection_name=table_name, id_array=ids)
        if status.OK():
            print("Delete top 10 vectors successfully")
        else:
            print(status.message)

    def search_vectors(self, table_name, q_vectors):
        search_params = {"metric_type": "IP", "params": {"nprobe": 32}}
        results = table_name.search(
            data=q_vectors, 
            anns_field="embeddings", 
            param=search_params, 
            limit=10,
            expr=None,
            consistency_level = 'Strong'
        )
        return results

    def delete_table(self, table_name):
        # Delete table
        status = self.client.drop_collection(table_name)
        if status.OK():
            print('Deleting table `{}` ... \n'.format(table_name))
            time.sleep(1)
        else:
            print(status.message)


def generate_clusters(embedding_file_path, graph_threshold, curation_threshold, table_name, csv_path):
    print('loading json!!!')
    embedding_file = open(embedding_file_path, "rb")
    primary_keys, embeddings = json.load(embedding_file)

    client = MilvusClient()
    client.connect()
    print("-------------Connected to the client-------------")

    client.create_table(table_name,len(embeddings[0]))
    collection = Collection(table_name)  # Get an existing collection.

    # ids = client.add_vectors(collection,primary_keys,embeddings)

    ids = primary_keys
    collection.load()

    n = 1

    chunks_ids = [ids[i:i+n] for i in range(0,len(ids),n)]
    chunks_vectors = [embeddings[i:i+n] for i in range(0,len(embeddings),n)]


    # # G = pickle.load(open('daily_report_graph.gpickle', 'rb'))

    # G = nx.Graph()
    chunk_no = 0
    # connected_nodes = []
    # final_file = [['xcall_id','cluster_id','distance']]
    final_file = [['test_id','nearest_ids_and_distances','time_for_searching']]
    for vectors in tqdm(chunks_vectors,desc='Progress'):
    #     print("Starting %sst/nd chunk of vectors"%(chunk_no+1))
        indexes = chunks_ids[chunk_no]

    #     print('\nSearching each vector in server ...')
        start_time = time.time()
        results = client.search_vectors(collection,vectors)
        end_time = time.time()
        total_search_time = end_time-start_time

        # print("Length of results %s"%(len(list(results))))
        # print("Length of indexes %s"%(len(indexes)))
        
        for id,result in zip(indexes,results):
            nearest_ids_distances = []
            # nearest_curation_ids, curation_distances = [],[]
            
            for node in result:
                nearest_ids_distances.append([node.id,node.distance])
            final_file.append([id,nearest_ids_distances,total_search_time])
        chunk_no += 1

    with open(csv_path,'w+') as f:
        f_writer = csv.writer(f)
        f_writer.writerows(final_file)
     
    