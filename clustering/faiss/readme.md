# Faiss Clustering

install necessary libraries from 
 *"requirements.txt"*

```
pip3 install -r requirements.txt
```


**run**


```
python3 faiss_clustering.py
```

**outputs**

CSV folder with top three predictions and its rescpective ground truths




