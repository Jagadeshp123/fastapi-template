import numpy as np 
import faiss  # this will import the faiss library
import pandas as pd
import os
import time
import yaml




start = time.time()



stream = open("params.yaml")
print(stream)
gen = yaml.load(stream, Loader=yaml.FullLoader)
print(gen["params"])
params = gen["params"]

## Write in params.yaml
accuracy_df = pd.DataFrame()
golden_signatures = params["golden_signatures"]
test = params["test"]
index_file = params["index_file"]
dimension = params["dimension"]
nlist = params["nlist"]
nprobe = params["nprobe"]
k = params["k"]
csv_path = params["csv_path"]


def load_embddings(req_npy_list,npy_folder_path):
    embd_list = []
    names_list = []
    for files in req_npy_list:
        npy = np.load(os.path.join(npy_folder_path,files))
        image_name = files.split("_")[0]
        embd_list.append(npy)
        names_list.append(image_name)
    return embd_list,names_list
print("Loading Golden Signatures...")
embd_load_start = time.time()
gol_sig = os.listdir(os.path.join(os.getcwd(),golden_signatures))
test_sig = os.listdir(os.path.join(os.getcwd(),test))
# print(gol_sig)
golden_signatures_embds_,golden_signatures_names_ = load_embddings(gol_sig,golden_signatures)
test_embds_,test_names_ = load_embddings(test_sig,test)


golden_signatures_embds = np.squeeze(np.array(golden_signatures_embds_))
golden_signatures_names = np.array(golden_signatures_names_)
print(golden_signatures_embds.shape)

test_embds = np.squeeze(np.array(test_embds_))
test_names = np.array(test_names_)
print(test_embds.shape)

embd_load_end = time.time()
print("Training starts...")
train_start = time.time()
# dimension = 1024    # dimensions of each vector                         
# n = 101    # number of vectors                   
np.random.seed(2023)             
# db_vectors = np.random.random((n, dimension)).astype('float32')
# print(db_vectors.shape)


# nlist = 3  # number of clusters
quantiser = faiss.IndexFlatL2(dimension)  
# index = faiss.IndexIVFFlat(quantiser, dimension, nlist,   faiss.METRIC_L2)
index = faiss.read_index(index_file)  # load the index
# print(index.is_trained)   # False
# index.train(golden_signatures_embds)  # train on the database vectors
# print(index.ntotal)   # 0
# index.add(golden_signatures_embds)   # add the vectors and update the index
print(index.is_trained)  # True
print(index.ntotal)   # 101

# faiss.write_index(index,"vector.index")  # save the index to disk


train_end = time.time()
# nprobe = 2  # find 2 most similar clusters
# n_query = 38  
# k = 3  # return 3 nearest neighbours
np.random.seed(0)   
print("Searching starts...")
search_start = time.time()
distances, indices = index.search(test_embds, k)
search_end = time.time()
# print(distances)
print(indices[0])
print((test_names[0]))
# print(golden_signatures_names[1])
# print(golden_signatures_names[18])
# print(golden_signatures_names[0])

accuracy_start = time.time()
i=0
for i in range(int(test_names.shape[0])):
    single_image_inference_start = time.time()
    print(i)
    # print(test_names[i].split("_")[0])
    # print(golden_signatures_names[indices[i]][0].split("_")[0])
    temp_df = pd.DataFrame()
    temp_df['test_image_id'] = [str(test_names[i].split("_")[0])]
    temp_df['predicted_id_1'] = [str(golden_signatures_names[indices[i]][0].split("_")[0])]
    temp_df['predicted_id_2'] = [str(golden_signatures_names[indices[i]][1].split("_")[0])]
    temp_df['predicted_id_3'] = [str(golden_signatures_names[indices[i]][2].split("_")[0])]



    # if (test_names[i].split("_")[0] == golden_signatures_names[indices[i]][0].split("_")[0]):
    #     temp_df = pd.DataFrame({test_names[i]:[int(1)]})
    # else:
    #     temp_df = pd.DataFrame({test_names[i]:[int(0)]})
    # single_image_inference_end = time.time()
    # print("Total single_image_inference Time is: "+str(single_image_inference_end - single_image_inference_start))
    # print(temp_df)
    accuracy_df = pd.concat([accuracy_df, temp_df],axis=1)
# accuracy_df_T = accuracy_df.T
accuracy_df.to_csv(csv_path)

accuracy_end = time.time()

end = time.time()
print("Total embd load Time is: "+str(embd_load_end - embd_load_start))
print("Total training Time is: "+str(train_end - train_start))
print("Total searching Time is: "+str(search_end - search_start))
print("Total accuracy calculation Time is: "+str(accuracy_end - accuracy_start))
print("Total Time is: "+str(end - start))
