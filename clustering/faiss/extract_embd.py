import tensorflow as tf
import os
import numpy as np
import pandas as pd
from glob import  glob

# import numpy as np
# import pandas as pd

# import umap
# from sklearn.datasets import fetch_openml
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt

# import seaborn as sns
import time
# import numpy as np
from PIL import Image
import os
# import pandas as pd
import sys
import shutil
import matplotlib
# from sklearn.cluster import DBSCAN
# import hdbscan
# import collections

import csv

# import yaml

np.set_printoptions(threshold=sys.maxsize)

# stream = open("params.yaml")
# print(stream)
# gen = yaml.load(stream, Loader=yaml.FullLoader)
# print(gen["params"])
# params = gen["params"]

# np.set_printoptions(threshold=sys.maxsize)

start_time = time.time()
embd_extract_start_time = time.time()
# import maha_vit
def pre_process(file):
    image = tf.io.read_file(file)
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.image.resize(image, [224, 224])
    # image = tf.image.convert_image_dtype(image, tf.float32)
    # image = tf.clip_by_value(image, 0., 1.)
    image = tf.expand_dims(image,0)

    return image


class WarmUpCosine(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(
        self, learning_rate_base=0.03, total_steps=13000, warmup_learning_rate=0.006, warmup_steps=10
    ):
        super(WarmUpCosine, self).__init__()

        self.learning_rate_base = learning_rate_base
        self.total_steps = total_steps
        self.warmup_learning_rate = warmup_learning_rate
        self.warmup_steps = warmup_steps
        self.pi = tf.constant(np.pi)

    def __call__(self, step):
        if self.total_steps < self.warmup_steps:
            raise ValueError("Total_steps must be larger or equal to warmup_steps.")
        learning_rate = (
            0.5
            * self.learning_rate_base
            * (
                1
                + tf.cos(
                    self.pi
                    * (tf.cast(step, tf.float32) - self.warmup_steps)
                    / float(self.total_steps - self.warmup_steps)
                )
            )
        )

        if self.warmup_steps > 0:
            if self.learning_rate_base < self.warmup_learning_rate:
                raise ValueError(
                    "Learning_rate_base must be larger or equal to "
                    "warmup_learning_rate."
                )
            slope = (
                self.learning_rate_base - self.warmup_learning_rate
            ) / self.warmup_steps
            warmup_rate = slope * tf.cast(step, tf.float32) + self.warmup_learning_rate
            learning_rate = tf.where(
                step < self.warmup_steps, warmup_rate, learning_rate
            )
        return tf.where(
            step > self.total_steps, 0.0, learning_rate, name="learning_rate"
        )
    def get_config(self):
        config = {
            'warmup_steps': self.warmup_steps,
            'total_steps': self.total_steps}
        return config

model_name = "cartzy_vitl16_v2_1981_skus_vid_frames_datasets_bs32_02-0.037601"
# model_name_1 = model_name.split("/")

# scheduled_lrs = WarmUpCosine(
#     learning_rate_base=INIT_LR,
#     warmup_learning_rate=WAMRUP_LR,
#     total_steps=1,
#     warmup_steps=WARMUP_STEPS,)
Reference = "test/"
# Analysis = "analysis/"

model = tf.keras.models.load_model(model_name,custom_objects={"WarmUpCosine": WarmUpCosine})
print(model.get_layer('keras_layer').output)
model_2 = tf.keras.Model(inputs=model.input, outputs= model.get_layer('keras_layer').output)
model.summary()
result = []
image_name_ref = []
image_name_ana = []
gt = []
embds_ref = []
embds_ana = []
# count = 0
ref_classes = os.listdir(Reference)
# ana_classes = os.listdir(Analysis)
# print(classes)
num_classes = len(ref_classes)
print(num_classes)
# filenames = glob(Reference + '/*/*')
# labels = [classes.index(name.split('/')[-2]) for name in filenames]
labels_np_ref = np.array(ref_classes)
# labels_np_ana = np.array(ana_classes)
# print(labels_np)
# print(labels_np.shape)
# for j in os.listdir(Reference):
#     folder = Reference+'/'+j
count = 0
for i in ref_classes:

    count = count+1
    gt.append(i)
    file_path_ref = Reference + i
    image_name_ref.append(i)
    # image_name_ana.append(j)
    image_tensor_ref = pre_process(file_path_ref)
    # image_tensor_ana = pre_process(file_path_ana)
    # var = model(image_tensor_ref)
    embd_ref = model_2(image_tensor_ref)

    embds_ref.append(embd_ref)

    # print(var)
    print(count)

# count = 0
# for j in ana_classes:

#     count = count+1
#     # gt.append(i)
#     file_path_ana = Analysis + j
#     image_name_ana.append(j)
#     image_tensor_ana = pre_process(file_path_ana)
#     # image_name_ana.append(j)

#     embd_ana = model_2(image_tensor_ana)
#     embds_ana.append(embd_ana)


#     print(count)

embds_np_ref = np.squeeze(np.array(embds_ref))
# embds_np_ana = np.squeeze(np.array(embds_ana))

# print(len(embds_np_ana))
# print(len(image_name_ana))
print(len(embds_np_ref))
print(len(image_name_ref))

np.save("test_embds.npy", embds_np_ref)
# np.save("ana_embd.npy",embds_np_ana)
np.save("test_names.npy", image_name_ref)
# np.save("ana_names.npy",image_name_ana)

# n_neighbors_list = params["n_neighbors_list"]
# min_dist_list = params["min_dist_list"]

# labels = np.zeros(27390,)


# embd_extract_end_time = time.time()
# data_load_start_time = time.time()
# # Loading ViT embeddings
# embeddings = np.load("ref_embd.npy")
# image_namess = np.load("ref_names.npy")
# image_data = pd.DataFrame(embeddings)

# data_load_end_time = time.time()

# for n in range(len(n_neighbors_list)):
# 	for md in range(len(min_dist_list)):
# 		# print(n, md)

# 		sns.set(context="paper", style="dark")
# 		print("panda")

# 		print("UMAP Starts...")
# 		umap_startTime = time.time()
# 		reducer = umap.UMAP(n_neighbors=int(n_neighbors_list[n]),
# 			min_dist=int(min_dist_list[md]),
# 			n_components=int(params["n_components"]), metric='euclidean')
# 		# print(reducer)
# 		embedding = reducer.fit(image_data)
# 		# test_embedding = embedding.transform(np.array(test_data))
# 		# print(embedding)
# 		# print((embedding.embedding_.T)*2)
# 		umap_endTime = time.time()
# 		colorCode = {0: '#15B01A', 1: '#FF6347', 2: '#808080', 3:'#FFFF00', 4:'#FF81C0', 5:'#580F41', 6:'#B56B16', 7:'#3016B5', 8: '#8375C9', 9:'#FF00A6',10:'#000000'}
# 		colorName = {0:'photoshop',1: 'real',2:'printcuts',3:'blur',4:'fake'}
# 		#### 0- green, 1-red, 2-grey, 3-yellow, 4-pink, 5-purple 
# 		plot_startTime = time.time()
# 		fig, ax = plt.subplots(figsize=(12, 10))
# 		labels_df = pd.DataFrame(labels, columns = ['classs'])
# 		coord = (embedding.embedding_.T) * 20
# 		plt.scatter(*embedding.embedding_.T,cmap="Spectral")
# 		plt.legend()
# 		plt.setp(ax, xticks=[], yticks=[])
# 		plt.gca().set_aspect('equal', 'datalim')

# 		plt.title("umap_finetuned_5class_customer - same", fontsize=18)
# 		# plt.colorbar(labels)
# 		print("pandaaaaaaaaaaaaaaaa")
# 		plt.savefig("umap_finetuned_5class_customer - same"+str(n_neighbors_list[n])+"_md"+str(min_dist_list[md])+".png")
# 		plt.close(fig)



# 		# plt.savefig("binary_ood/umap_finetuned_5class_ 5ood_ep5"+str(n_neighbors_list[n])+"_md"+str(min_dist_list[md])+".png")
# 		plot_endTime = time.time()



# 		print("UMAP Operation Time is: "+str(umap_endTime - umap_startTime))
# 		print("Plotting Time is: "+str(plot_endTime - plot_startTime))



# coordinates = embedding.embedding_

# umap_dict = {}
# df = pd.DataFrame(coordinates)
# X = df

# clustering = DBSCAN(eps=params["eps"], min_samples=params["min_samples"]).fit(X)
# # clustering = hdbscan.HDBSCAN(min_cluster_size=params["min_cluster_size"]).fit(X)
# # print(max(clustering.labels_))

# # print(collections.Counter(clustering.labels_))

# output_labels = pd.DataFrame(image_namess)
# # print(output_labels)
# output_labels.columns = ['image_name']
# output_labels.insert(1, 'labels', clustering.labels_)
# # print(output_labels.head)
# path = os.getcwd()
# output_labels.to_csv(os.path.join(path,params['input_csv_path']), index=False, header=False)
# os.makedirs(os.path.join(path,params['output_folder_path']) ,exist_ok = True)
# input_csv_path = os.path.join(path,params['input_csv_path'])

# input_images_folder_path = os.path.join(path,params['input_images_folder_path'])
# output_folder_path = os.path.join(path,params['output_folder_path'])
# count = 0
# with open(input_csv_path, 'r') as fr:
#     reader = csv.reader(fr, delimiter=',')
#     for each_row in reader:
#         # print(os.path.join(output_folder_path, str(each_row[1])))
#         count += 1
#         # print(count)
#         os.makedirs(os.path.join(output_folder_path, str(each_row[1])),exist_ok = True)
#         os.system('cp ' + os.path.join(input_images_folder_path, each_row[0]) + ' ' + os.path.join(output_folder_path, each_row[1]) + '/')
# fr.close()
# print('Bucketing is done')


# end_time = time.time()
# print("Total Time is: "+str(end_time - start_time))


