from argparse import ArgumentParser
import torch
import os

def main():
    parser = ArgumentParser()
    parser.add_argument("--yolo_model_path")
    args = parser.parse_args()
    os.chdir('detection')
    print(os.getcwd())

    w = args.yolo_model_path
    ckpt = torch.load(w, map_location='cpu')
    return "done"

if __name__ == "__main__":
    main()