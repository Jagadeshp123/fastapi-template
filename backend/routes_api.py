from detection.src import api_detection
from recognition import api_recognition
# from clustering.faiss import api_faiss_clustering
# from clustering.milvus import api_milvus_clustering
from fastapi import APIRouter, Form
from typing import Optional
import uvicorn

app = APIRouter()

@app.get('/detection')
def detection_app(algorithm: Optional[str] = Form(), video_path: Optional[str] = Form(), model_path:Optional[str] = Form(None), class_label:Optional[str] = None, skip_inference:Optional[bool] = False,
         visualize_results:Optional[bool] = False, no_bb_to_save:Optional[int] = 10, yolo_save_every_frame:Optional[bool] = False, n_of_sharp_imgs:Optional[str] = 4):
         api_detection.detection(algorithm, video_path, model_path, class_label, skip_inference,
         visualize_results, no_bb_to_save, yolo_save_every_frame, n_of_sharp_imgs)


# @app.get('/recognition')
# def recognition_app(model: str = Form(...), images: str = Form(...), npy_folder_name: str = Form(...), labels_csv: str = Form(...)):
#     api_recognition.rec_func(model=model,images=images,npy_folder_name=npy_folder_name,labels_csv=labels_csv)


# @app.get('/clustering/faiss')
# def faiss_clustering_app():
#     api_faiss_clustering.faiss()


# @app.get('/clustering/milvus')
# def milvus_clustering_app():
#     api_milvus_clustering.milvus()    

# if __name__ == "__main__":
#     uvicorn.run(app, host="0.0.0.0", port=8000)

# if __name__ == "__main__":
#     main()
