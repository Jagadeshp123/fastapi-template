from typing import List, Optional

from pydantic import BaseModel


class ItemBase(BaseModel):
    brand: str
    item_name : str
    mrp: float
    discount: int
    f_price: float


class ItemCreate(ItemBase):
    pass


class Item(ItemBase):
    id: int

    class Config:
        orm_mode = True

