from sqlalchemy.orm import Session

from . import models, schemas


class ItemRepo:
    
        async def create(db: Session, item: schemas.ItemCreate):
                db_item = models.Item(brand=item.brand,item_name=item.item_name,mrp=item.mrp,discount=item.discount,f_price=item.f_price)
                db.add(db_item)
                db.commit() 
                db.refresh(db_item)
                return db_item

        def fetch_by_name(db: Session,brand):
                return db.query(models.Item).filter(models.Item.brand == brand).first()
        