from sqlalchemy import Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base=declarative_base()



class Item(Base):
    __tablename__ = "items"
    
    id = Column(Integer, primary_key=True,index=True)
    brand=Column(String(10),nullable=True,unique=False,index=True)
    item_name = Column(String(80),unique=False)
    mrp = Column(Float(precision=2))
    discount=Column(Integer,nullable=False)
    f_price=Column(Float(precision=2),nullable=False)
    def __repr__(self):
        return 'ItemModel(brand=%s, item_name=%s,mrp=%s)' % (self.brand, self.item_name,self.mrp)
