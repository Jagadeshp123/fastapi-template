from fastapi import FastAPI
import uvicorn


app = FastAPI()

from backend import routes_api
# from recognition import api_recognition
# from detection.src import *
# from detection import *

# app.include_router(api_recognition.app)
# app.include_router(api_detection.app)
app.include_router(routes_api.app)

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=5000) 