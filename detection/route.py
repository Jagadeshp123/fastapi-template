from argparse import ArgumentParser
import os
import cv2

from src.dense_optical_flow import dense_optical_flow, split_video, full_pipeline_demo, dense_optical_flow_with_fsl, combined_yolo_optical_flow
from src.lucas_kanade import lucas_kanade_method

from fastapi import FastAPI, Form
from typing import Optional

app = FastAPI()

@app.get('/detection')
def main(algorithm: Optional[str] = Form(), video_path: Optional[str] = Form(), model_path:Optional[str] = Form(None), yolo_model_path:Optional[str] = Form(), class_label:Optional[str] = None, skip_inference:Optional[bool] = True,
         visualize_results:Optional[bool] = False, no_bb_to_save:Optional[int] = 10, yolo_save_every_frame:Optional[bool] = False, n_of_sharp_imgs:Optional[str] = 4):
    print("l16")
    print(algorithm, video_path, model_path, yolo_model_path, type(class_label), type(skip_inference), type(visualize_results), type(no_bb_to_save), type(yolo_save_every_frame), n_of_sharp_imgs)
    print("skip_inteface",type(skip_inference))
    # parser = ArgumentParser()
    # parser.add_argument(
    #     "--algorithm",
    #     choices=["farneback", "lucaskanade", "lucaskanade_dense", "rlof", "tvl1", "split_video",
    #              "full_pipeline_demo", "lucaskanade_dense_with_fsl", "combined_yolo_optical_flow"],
    #     required=True,
    #     help="Optical flow algorithm to use",
    # )
    # parser.add_argument("--video_path", help="Path to the video")
    # parser.add_argument("--model_path", help="Path to the few-shot learner model .pt file")
    # parser.add_argument("--yolo_model_path", help="Path to the yolo-v7 object detection model .pt file")
    # parser.add_argument("--class_label", help="Class label of the object of interest in input video")
    # parser.add_argument("--skip_inference", action="store_true", help="If specified, FSL inference is skipped")
    # parser.add_argument("--visualize_results", action="store_true", help="If specified, FSL inference is skipped")
    # parser.add_argument('--no_bb_to_save', action="store", type=int, required=False, default=10,
    #                     help="Number of bounding boxes to save at the end of optical-flow based object detection")
    # parser.add_argument("--yolo_save_every_frame", action="store_true", help="If specified, the result for every frame "
    #                                                                          "will be saved during yolo-optical flow "
    #                                                                          "combined object detection")
    # parser.add_argument('--n_of_sharp_imgs', action="store", type=int, required=False, default=4,
    #                     help="Number of least blurry images to pick from YOLO outputs")

    os.makedirs('./cropped_bbs', exist_ok=True)

    # args = parser.parse_args()
    # video_path = args.video_path
    # model_path = args.model_path
    # yolo_model_path = args.yolo_model_path
    # class_label = args.class_label
    # skip_inference = args.skip_inference
    # visualize_results = args.visualize_results
    # no_bb_to_save = args.no_bb_to_save
    # yolo_save_every_frame = args.yolo_save_every_frame
    # n_of_sharp_imgs = args.n_of_sharp_imgs
    if model_path is not None:
        print(f"Few shot learner model will be loaded from: {model_path}")
    if yolo_model_path is not None:
        print(f"YOLO model will be loaded from: {yolo_model_path}")
        print(f"Number of sharp bounding boxes to pick from YOLO outputs: {n_of_sharp_imgs}")

    if algorithm == "lucaskanade":
        lucas_kanade_method(video_path)
    elif algorithm == "lucaskanade_dense":
        # gridStep  k   sigma  use_post_proc fgs_lambda fgs_sigma
        params = [4, 200, 0.05, True, 500.0, 1.5]
        method = cv2.optflow.calcOpticalFlowSparseToDense
        dense_optical_flow(method, video_path, params, to_gray=True)
    elif algorithm == "lucaskanade_dense_with_fsl":
        # gridStep  k   sigma  use_post_proc fgs_lambda fgs_sigma
        params = [4, 200, 0.05, True, 500.0, 1.5]
        method = cv2.optflow.calcOpticalFlowSparseToDense
        dense_optical_flow_with_fsl(method, video_path, model_path, class_label, params, to_gray=True)
    elif algorithm == "full_pipeline_demo":
        # INCREASE 'GRIDSTEP' AND/OR REDUCE 'K' FOR FASTER OPTICAL FLOW
        # gridStep  k   sigma  use_post_proc fgs_lambda fgs_sigma
        params = [4, 200, 0.05, True, 500.0, 1.5]
        method = cv2.optflow.calcOpticalFlowSparseToDense
        full_pipeline_demo(method, video_path, model_path, class_label, visualize_results, no_bb_to_save,
                           skip_inference, params, resolution=(720, 1280), to_gray=True)

        # method = cv2.optflow.DualTVL1OpticalFlow_create()
        # method.setLambda(0.1)
        # full_pipeline_demo(method, video_path, model_path, class_label, to_gray=True, tvl1=True)
    elif algorithm == "combined_yolo_optical_flow":
        # INCREASE 'GRIDSTEP' AND/OR REDUCE 'K' FOR FASTER OPTICAL FLOW
        # gridStep  k   sigma  use_post_proc fgs_lambda fgs_sigma
        params = [4, 200, 0.05, True, 500.0, 1.5]
        method = cv2.optflow.calcOpticalFlowSparseToDense
        combined_yolo_optical_flow(method, video_path, model_path, yolo_model_path, class_label, visualize_results,
                                   no_bb_to_save, skip_inference, yolo_save_every_frame, n_of_sharp_imgs,
                                   params, resolution=(720, 1280), to_gray=True)
    elif algorithm == "farneback":
        method = cv2.calcOpticalFlowFarneback
        params = [0.5, 3, 15, 3, 5, 1.2, 0]  # Farneback's algorithm parameters
        dense_optical_flow(method, video_path, params, to_gray=True)
    elif algorithm == "rlof":
        method = cv2.optflow.calcOpticalFlowDenseRLOF
        dense_optical_flow(method, video_path)
    elif algorithm == "tvl1":
        method = cv2.optflow.DualTVL1OpticalFlow_create()
        method.setLambda(0.1)
        dense_optical_flow(method, video_path, to_gray=True, tvl1=True)
        # dense_optical_flow_with_fsl(method, video_path, model_path, class_label, to_gray=True, tvl1=True)

    elif algorithm == "split_video":
        split_video(video_path)


# if __name__ == "__main__":
#     main()
