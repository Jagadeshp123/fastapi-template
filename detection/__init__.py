
# from .src import api_detection
# from .src import dense_optical_flow
# from .src import utils
import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.realpath('.'),'.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


file_path = os.path.dirname(__file__)

yolo_model_path = os.getenv('yolo_model_path')

yolo_model_path1 = os.path.join(file_path, yolo_model_path)
