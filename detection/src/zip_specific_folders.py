import zipfile
import os

os.makedirs("../../360_video_data_till_nov_22_SPECIFIC", exist_ok=True)

txt_file = open("../datasets/combined_130_class_grocery_dataset_2022_11_21/test/dirs.txt", "r")
folder_names = txt_file.read()

archive = zipfile.ZipFile('../../360_video_data_till_nov_22.zip')
for file in archive.namelist():
    img_head, img_tail = os.path.split(file)
    class_label = os.path.basename(os.path.normpath(img_head))
    print(class_label)
    if class_label in folder_names:
        archive.extract(file, '../../360_video_data_till_nov_22_SPECIFIC')
