import os
import cv2
import torch
import argparse
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from torchvision import transforms

# from data_handler import SquarePad
from detection.src.data_handler import SquarePad
from detection.src.fsl_models import TripletNet, EmbeddingNet
# from fsl_models import TripletNet, EmbeddingNet


def inference(model_path, input_image, annotation_name=None, force_cpu=False, cuda_id=0, show_plot=True, verbose=True):
    """
    Inference API for the few shot learner object recognition model

    :param model_path: Path to the previously saved few shot learner model checkpoint
    :param input_image: Path to the input image or an opencv image array
    :param annotation_name: Optional; Class name the object in the input image belongs to
    :param force_cpu: Optional; If True, inference will be on CPU
    :param cuda_id: Optional; Specified which GPU to use for inference, e.g: 0, 1, 2
    :param show_plot: Optional; If True, plots the input image
    :param verbose: Optional; If True, prints logging messages in console
    :return: top1 prediction class name, ordered prediction list where each entry is as follows: [class label, distance]
    """

    device = torch.device(f"cuda:{cuda_id}" if torch.cuda.is_available() and not force_cpu else 'cpu')
    if verbose:
        if device.type != 'cpu':
            print(f"Available device: {device.type}, ID: {cuda_id}")
        else:
            print(f"Available device: {device.type}")

    model_checkpoint = torch.load(model_path, map_location=device)
    # class_number = model_checkpoint['class_number']
    input_image_size = model_checkpoint['input_image_size']
    latent_vector_size = model_checkpoint['latent_vector_size']
    class_prototypes = model_checkpoint['class_prototypes']
    idx_to_class_label = model_checkpoint['idx_to_class_label']

    embedding_net = EmbeddingNet(input_image_size, latent_vector_size)
    model = TripletNet(embedding_net)
    model.to(device)
    model.load_state_dict(model_checkpoint['model_model'])

    with torch.no_grad():
        model.eval()
        transform = transforms.Compose([
            SquarePad(),
            transforms.Resize(size=[input_image_size, input_image_size]),  # resize image
            transforms.ToTensor(),  # transform to tensor and rescale to the range [0, 1]
        ])
        label_info_available = False if annotation_name in [None, "None"] else True
        # Check if input image was a path
        if isinstance(input_image, str):
            image = Image.open(input_image).convert('RGB')
            img_path = os.path.basename(input_image)
        else:
            image = input_image
            image = Image.fromarray(image)
            img_path = "Loaded from image array"

        image = transform(image)
        image = image.unsqueeze_(0)
        image = image.to(device)

        embedding = model.get_embedding(image).data.cpu().numpy()

        all_distances = []

        for class_num in range(0, len(class_prototypes)):
            if isinstance(class_prototypes[class_num], list):
                class_prototypes[class_num] = np.reshape(
                    np.asarray(class_prototypes[class_num]), (-1, np.asarray(class_prototypes[class_num]).shape[-1]))
            distances = np.power(embedding - class_prototypes[class_num], 2).sum(1)

            centroid_idx = np.argmin(distances)

            all_distances.append([idx_to_class_label[class_num], distances[centroid_idx]])

        all_distances.sort(key=lambda x: x[1])
        top1_prediction_class_name = all_distances[0][0]

        if show_plot:
            plt.imshow(image.squeeze(0).detach().cpu().permute(1, 2, 0))
            plt.show()
        if verbose:
            if label_info_available:
                is_correct = "CORRECT top25" if \
                    any(annotation_name in elem for elem in all_distances[:25]) else "INCORRECT top25"
                # is_correct = "CORRECT" if top1_prediction_class_name == annotation_name else "INCORRECT"
                print(f"\n{is_correct}: "
                      f"Input image: {img_path}, "
                      f"annotation: {annotation_name}, "
                      f"top1 prediction: {top1_prediction_class_name}")
                print("Top 25 predictions with distances.", all_distances[:25])
            else:
                print(f"Top1 prediction: {top1_prediction_class_name}")
                print("Top 25 predictions with distances.", all_distances[:25])

    return top1_prediction_class_name, all_distances


def multi_frame_inference(model_path, input_image_list, annotation_name=None,
                          force_cpu=False, cuda_id=0, show_plot=True, verbose=True):
    """
    Multi-Frame Inference API for the few shot learner object recognition model

    :param model_path: Path to the previously saved few shot learner model checkpoint
    :param input_image_list: Input image LIST where each image is an opencv image array OR path to folder of images
    :param annotation_name: Optional; Class name the object in the input image belongs to
    :param force_cpu: Optional; If True, inference will be on CPU
    :param cuda_id: Optional; Specified which GPU to use for inference, e.g: 0, 1, 2
    :param show_plot: Optional; If True, plots the input image
    :param verbose: Optional; If True, prints logging messages in console
    :return: top1 prediction class name, ordered prediction list where each entry is as follows: [class label, distance]
    """

    if isinstance(input_image_list, str):
        if os.path.isdir(input_image_list):
            paths = [os.path.join(input_image_list, f) for f in os.listdir(input_image_list)]
            input_image_list = []
            for path in paths:
                if not os.path.isdir(path):
                    image = cv2.imread(path)
                    input_image_list.append([image, 0])
        else:
            raise OSError("Input should be either an array of images or a path to a directory containing the images")

    device = torch.device(f"cuda:{cuda_id}" if torch.cuda.is_available() and not force_cpu else 'cpu')
    if verbose:
        if device.type != 'cpu':
            print(f"Available device: {device.type}, ID: {cuda_id}")
        else:
            print(f"Available device: {device.type}")

    model_checkpoint = torch.load(model_path, map_location=device)
    # class_number = model_checkpoint['class_number']
    input_image_size = model_checkpoint['input_image_size']
    latent_vector_size = model_checkpoint['latent_vector_size']
    class_prototypes = model_checkpoint['class_prototypes']
    idx_to_class_label = model_checkpoint['idx_to_class_label']

    embedding_net = EmbeddingNet(input_image_size, latent_vector_size)
    model = TripletNet(embedding_net)
    model.to(device)
    model.load_state_dict(model_checkpoint['model_model'])

    with torch.no_grad():
        model.eval()
        transform = transforms.Compose([
            SquarePad(),
            transforms.Resize(size=[input_image_size, input_image_size]),  # resize image
            transforms.ToTensor(),  # transform to tensor and rescale to the range [0, 1]
        ])
        label_info_available = False if annotation_name in [None, "None"] else True

        img_path = "Loaded from image array list"

        all_distances = {}
        for image in input_image_list:
            image = transform(Image.fromarray(cv2.cvtColor(image[0], cv2.COLOR_BGR2RGB)))
            image = image.unsqueeze_(0).to(device)
            embedding = model.get_embedding(image).data.cpu().numpy()
            for class_num in range(0, len(class_prototypes)):
                if isinstance(class_prototypes[class_num], list):
                    class_prototypes[class_num] = np.reshape(
                        np.asarray(class_prototypes[class_num]), (-1, np.asarray(class_prototypes[class_num]).shape[-1]))
                label = idx_to_class_label[class_num]
                distances = np.power(embedding - class_prototypes[class_num], 2).sum(1)

                centroid_idx = np.argmin(distances)
                all_distances[label] = all_distances.setdefault(label, 0) + distances[centroid_idx]

            if show_plot:
                plt.imshow(image.squeeze(0).detach().cpu().permute(1, 2, 0))
                plt.show()

        all_distances = sorted(all_distances.items(), key=lambda item: item[1])

        if verbose:
            if label_info_available:
                is_correct = "CORRECT top25" if \
                    any(annotation_name in elem for elem in all_distances[:25]) else "INCORRECT top25"
                # is_correct = "CORRECT" if top1_prediction_class_name == annotation_name else "INCORRECT"
                print(f"\n{is_correct}: "
                      f"Input image: {img_path}, "
                      f"annotation: {annotation_name}, "
                      f"prediction: {all_distances[0][0]}, "
                      f"with accumulated distance: {all_distances[0][1]}")
                print("Top 25 predictions with distances.", all_distances[:25])
            else:
                print(f"Prediction: {all_distances[0][0]}, "
                      f"with accumulated distance: {all_distances[0][1]}")
                print("Top 25 predictions with distances.", all_distances[:25])

    return all_distances[0][0], all_distances


def main():
    # Argument parsing
    parser = argparse.ArgumentParser(description='Object classification API')
    parser.add_argument('--verbose', action="store_true",
                        help="Output log information")
    parser.add_argument('--input_image', action="store", type=str, required=True,
                        help="Path to input image file or input image array")
    parser.add_argument('--annotation_name', action="store", type=str, required=False,
                        help="Annotation class name of the input image file or input image array")
    parser.add_argument('--model_path', action="store", type=str, required=True,
                        help="Path to the saved model checkpoint to load")
    parser.add_argument('--force_cpu', action="store_true",
                        help="Forces target device to be CPU.")
    parser.add_argument('--cuda_id', action="store", type=int, required=False, default=0,
                        help="ID of the GPU device")
    parser.add_argument('--show_plot', action="store_true",
                        help="Show input image and print results")

    args = parser.parse_args()

    top1_prediction_class_name, all_predictions_sorted = \
        inference(args.model_path, args.input_image, args.annotation_name,
                  args.force_cpu, args.cuda_id, args.show_plot, args.verbose)


if __name__ == '__main__':
    main()
