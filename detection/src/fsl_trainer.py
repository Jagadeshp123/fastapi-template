import os
import torch
import random
import argparse
import numpy as np
import collections
from PIL import Image
from tqdm import tqdm
import torch.optim as optim
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from torchvision import transforms
from torch.utils.data import DataLoader
from matplotlib.offsetbox import OffsetImage, AnnotationBbox


from losses import TripletLossHardMining, TripletLossSemiHardMining
from fsl_models import TripletNet, EmbeddingNet
from data_handler import SquarePad, CustomDataset, CustomSubset, load_dataset_from_folder


def get_color_map(n, name='hsv'):
    """
    Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name
    """
    color_map = plt.cm.get_cmap(name, n)
    shuffled_color_map = []
    for i in range(n):
        shuffled_color_map.append(color_map(i))

    random.shuffle(shuffled_color_map)

    return shuffled_color_map


def plot_embeddings(images, embeddings, targets, legend, colors, class_number,
                    idx_to_class_label, mode="train", xlim=None, ylim=None, plot_images=True):
    """
    Plots extracted image embeddings to 2D after a TSNE dimensionality reduction

    :param images: Numpy array of input images
    :param embeddings: Numpy array of extracted embeddings
    :param targets: Numpy array of input labels
    :param legend: Class names for plotting on the 2D scatter plot as legend
    :param colors: List of colors specific for each class in the dataset
    :param class_number: Number of classes in the dataset
    :param idx_to_class_label: Dictionary that maps the class ids to class names
    :param mode: Optional; name (e.g.: "train", "test") for saving the final 2D scatter plot
    :param xlim: Optional; max. width for scatter plot
    :param ylim: Optional; max. height for scatter plot
    :param plot_images: Optional; if True, plots resized mini versions of input images onto 2D scatter plot
    """
    # Reduce dimensionality of the embedded vectors to 2
    tsne = TSNE(n_components=2, verbose=0, perplexity=100, n_iter=5000)
    tsne_results = tsne.fit_transform(embeddings)
    embeddings = tsne_results
    # Plot the 2D embedded vectors
    fig, ax = plt.subplots(figsize=(class_number, class_number))
    for i in range(class_number):
        inds = np.where(targets == i)[0]
        class_embeddings_x = embeddings[inds, 0]
        class_embeddings_y = embeddings[inds, 1]
        cluster_center_x = np.mean(embeddings[inds, 0])
        cluster_center_y = np.mean(embeddings[inds, 1])
        if plot_images:
            for j in range(0, len(images[inds])):
                ab = AnnotationBbox(OffsetImage(np.clip(images[inds][j].transpose(1, 2, 0), 0, 1), zoom=0.1),
                                    (class_embeddings_x[j], class_embeddings_y[j]), frameon=True,
                                    bboxprops=dict(edgecolor=colors[i]), pad=0.05)
                ax.add_artist(ab)
                plt.plot([cluster_center_x, class_embeddings_x[j]],
                         [cluster_center_y, class_embeddings_y[j]],
                         marker='o', color=colors[i], alpha=0.2)
                # do not plot more than 10 images / class
                if j >= 10:
                    break

        plt.scatter(class_embeddings_x, class_embeddings_y, color=colors[i])
        plt.text(cluster_center_x, cluster_center_y,
                 f"class: {idx_to_class_label[i]}", fontsize=12, color=colors[i])
    if xlim:
        plt.xlim(xlim[0], xlim[1])
    if ylim:
        plt.ylim(ylim[0], ylim[1])
    plt.legend(legend)
    fig.savefig(f"../saved_plots/2d_projection_{mode}.png", dpi=fig.dpi)


def extract_embeddings(dataloader, model, device, latent_vector_size, input_image_size):
    """
    Extracts embeddings from input images

    :param dataloader: Image data loader
    :param model: Trained few shot learner model
    :param device: Device to run the computations on
    :param latent_vector_size: Size of the latent vector (specified by the few shot learner model)
    :param input_image_size: Size of the input images (specified by the few shot learner model)
    :return: Numpy arrays of: extracted embeddings, input images, input labels
    """
    # Extract the embeddings of the images in the data loader
    with torch.no_grad():
        model.eval()
        embeddings = np.zeros((len(dataloader.dataset), latent_vector_size))
        labels = np.zeros(len(dataloader.dataset))
        images = np.zeros((len(dataloader.dataset), 3, input_image_size, input_image_size))
        k = 0
        for img, label in tqdm(dataloader):
            img = img.to(device)
            embeddings[k:k + len(img)] = model.get_embedding(img).data.cpu().numpy()
            labels[k:k + len(label)] = label.cpu().numpy()
            images[k:k + len(img)] = img.cpu().numpy()
            k += len(img)
    return embeddings, images, labels


def save_checkpoint(epoch_num, model, class_number, input_image_size,
                    latent_vector_size, class_prototypes, idx_to_class_label, optimizer=None, final=False):
    """
    Saving a checkpoint at the end or during the training

    Args:
        model: The model to be saved
        class_number: Number of classes in the dataset the model was trained on
        input_image_size: Size of input image (same as specified during training)
        latent_vector_size: Size of latent vectors (same as specified during training)
        class_prototypes: Calculated class cluster center vector prototypes
        idx_to_class_label: Class id to Class label mapper dictionary
        epoch_num: Number of epochs the model has already been trained for
        optimizer: Optional; Optimizer of the models
        final: if True: saving the models/checkpoint at the end of the training
               if False: saving the models with their corresponding epoch_num (saving checkpoint during training)
    """
    if final:
        filename_extension = "_final.pt"
    else:
        filename_extension = f"_{epoch_num}_epoch.pt"

    torch.save({
        'epoch': epoch_num,
        'class_number': class_number,
        'input_image_size': input_image_size,
        'latent_vector_size': latent_vector_size,
        'class_prototypes': class_prototypes,
        'idx_to_class_label': idx_to_class_label,
        'model_model': model.state_dict(),
    }, '../saved_models/fsl_model_checkpoint' + filename_extension)

    if optimizer is not None:
        torch.save({
            'optimizer_state_dict': optimizer.state_dict(),
        }, '../saved_models/fsl_model_optimizer_checkpoint' + filename_extension)


def load_checkpoint_and_models(device, args, mode="eval"):
    """
    Loading a previously saved checkpoint

    Args:
        device: Device to load the checkpoints to, e.g.: "cpu", "cuda:0"
        args: Arguments of the script
        mode: If "eval": switch to evaluation mode, if "train": switch to training mode
    Returns: model, model details extracted from the checkpoint,
     number of epochs the training can be continued from, optimizer (if mode == "train")
    """
    # if "final", then the final checkpoint is loaded
    # if any other number, then the checkpoint corresponding to that epoch_n is loaded
    if args.model_checkpoint == "final":
        filename_extension = "_final.pt"
    else:
        filename_extension = f"_{args.model_checkpoint}_epoch.pt"

    model_checkpoint = torch.load("../saved_models/fsl_model_checkpoint" + filename_extension,
                                  map_location=device)

    model_details = {
        "class_number": model_checkpoint['class_number'],
        "input_image_size": model_checkpoint['input_image_size'],
        "latent_vector_size": model_checkpoint['latent_vector_size'],
        "class_prototypes": model_checkpoint['class_prototypes'],
        "idx_to_class_label": model_checkpoint['idx_to_class_label'],
        "epoch_num": model_checkpoint['epoch']
    }

    embedding_net = EmbeddingNet(model_details["input_image_size"], model_details["latent_vector_size"])
    model = TripletNet(embedding_net)
    model.to(device)
    model.load_state_dict(model_checkpoint['model_model'])

    epoch_num = model_checkpoint['epoch']

    print(f"Checkpoint loaded after {epoch_num} epochs")
    if mode == "eval":
        # Switching the model to evaluation mode
        model.eval()
        return model, model_details, int(epoch_num) + 1
    elif mode == "train":
        # Switching the model to training mode
        optimizer = optim.Adam(model.parameters(), lr=args.lr)
        optimizer_checkpoint = torch.load("../saved_models/fsl_model_optimizer_checkpoint" + filename_extension,
                                          map_location=device)
        optimizer.load_state_dict(optimizer_checkpoint['optimizer_state_dict'])
        model.train()
        return model, model_details, int(epoch_num) + 1, optimizer


def calculate_class_prototypes(dataset, label_to_indices, model, device, img_size, use_random_center_crops):
    """
    Calculated class prototypes based on training dataset and trained model checkpoint

    :param dataset: Training dataset
    :param label_to_indices: Dictionary which maps class labels to the
     corresponding indices of samples in the training dataset
    :param model: Trained few shot learner model checkpoint
    :param device: Specifies where computations will be carried out, e.g.: "cuda:0", "cpu"
    :param img_size: Size of the input image for model
    :param use_random_center_crops: Use random center crops during augmentation - influences resizing
    :return: Dictionary containing class ids as keys and class center vector prototypes as values
    """
    class_embeddings = collections.defaultdict(list)
    only_resize = transforms.Resize(size=[img_size, img_size])

    for class_id in tqdm(range(0, len(label_to_indices))):
        if len(label_to_indices[class_id]) == 0:
            continue
        lower_index, upper_index = label_to_indices[class_id][0], label_to_indices[class_id][1]
        for index in range(lower_index, upper_index):
            image = dataset[index][0].to(device)
            image = image[np.newaxis, :]
            if use_random_center_crops:
                image = only_resize(image)
            embedding = model.get_embedding(image)
            class_embeddings[class_id].append(embedding.data.cpu().numpy())

    class_embeddings = {class_id: np.mean(embeddings, axis=0) for class_id, embeddings in class_embeddings.items()}

    return class_embeddings


def calculate_accuracy_max_10_per_class(dataset, label_to_indices, idx_to_class_label, model_idx_to_class_label,
                                        class_prototypes, use_random_center_crops, img_size, model, device):
    """
    Calculate accuracy for 10 samples per class (short test accuracy calculation for quick verification)
    Logs Top1, Top5, Top10 and Top25 accuracies

    :param dataset: Test dataset
    :param label_to_indices: Dictionary that maps classes in the test dataset to corresponding indices of test samples
    :param idx_to_class_label: Dictionary that maps class ids to class labels
    :param model_idx_to_class_label: Dictionary that maps class ids to class labels - for the trained model
    :param class_prototypes: Dictionary containing class ids as keys and class center vector prototypes as values
    :param use_random_center_crops: Use random center crops during augmentation - influences resizing
    :param img_size: Size of the input image for model
    :param model: Trained few shot learner model
    :param device: Specifies where computations will be carried out, e.g.: "cuda:0", "cpu"
    """
    with torch.no_grad():
        correct = 0
        correct_top5 = 0
        correct_top10 = 0
        correct_top25 = 0
        total = 0
        only_resize = transforms.Resize(size=[img_size, img_size])
        model.eval()
        for class_id in tqdm(range(0, len(label_to_indices))):
            if len(label_to_indices[class_id]) == 0:
                continue
            lower_index, upper_index = label_to_indices[class_id][0], label_to_indices[class_id][1]
            for i, index in enumerate(range(lower_index, upper_index)):
                image = dataset[index][0].to(device)
                label = dataset[index][1]
                image = image[np.newaxis, :]
                if use_random_center_crops:
                    image = only_resize(image)
                embedding = model.get_embedding(image).data.cpu().numpy()

                all_distances = []
                for class_num in range(0, len(class_prototypes)):
                    distance = np.power(embedding - class_prototypes[class_num], 2).sum(1)
                    all_distances.append([model_idx_to_class_label[class_num], distance[0]])

                all_distances.sort(key=lambda x: x[1])

                if idx_to_class_label[label] in all_distances[0]:
                    correct += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:5]):
                    correct_top5 += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:10]):
                    correct_top10 += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:25]):
                    correct_top25 += 1

                total += 1
                if i >= 10:
                    break

    print(f"Top1 Accuracy: {correct / total * 100:.2f} %")
    print(f"Top5 Accuracy: {correct_top5 / total * 100:.2f} %")
    print(f"Top10 Accuracy: {correct_top10 / total * 100:.2f} %")
    print(f"Top25 Accuracy: {correct_top25 / total * 100:.2f} %")


def calculate_multi_frame_accuracy_for_limited_no_samples(dataset, label_to_indices, idx_to_class_label,
                                                          model_idx_to_class_label,  class_prototypes,
                                                          use_random_center_crops, img_size, model, device):
    """
    Calculate multi-frame (3 images per batch) accuracy for limited batches (120 batches) per each class
    (short test accuracy calculation for quick verification)
    Logs Top1, Top5, Top10 and Top25 accuracies

    :param dataset: Test dataset
    :param label_to_indices: Dictionary that maps classes in the test dataset to corresponding indices of test samples
    :param idx_to_class_label: Dictionary that maps class ids to class labels
    :param model_idx_to_class_label: Dictionary that maps class ids to class labels - for the trained model
    :param class_prototypes: Dictionary containing class ids as keys and class center vector prototypes as values
    :param use_random_center_crops: Use random center crops during augmentation - influences resizing
    :param img_size: Size of the input image for model
    :param model: Trained few shot learner model
    :param device: Specifies where computations will be carried out, e.g.: "cuda:0", "cpu"
    """
    with torch.no_grad():
        correct = 0
        correct_top5 = 0
        correct_top10 = 0
        correct_top25 = 0
        total = 0
        only_resize = transforms.Resize(size=[img_size, img_size])
        model.eval()
        for class_id in tqdm(range(0, len(label_to_indices))):
            if len(label_to_indices[class_id]) == 0:
                continue
            lower_index, upper_index = label_to_indices[class_id][0], label_to_indices[class_id][1]
            for i, index in enumerate(range(lower_index, upper_index)):
                index = np.random.choice(range(lower_index, upper_index))
                image1 = dataset[index][0].to(device)
                image2 = dataset[np.random.choice(range(lower_index, upper_index))][0].to(device)
                image3 = dataset[np.random.choice(range(lower_index, upper_index))][0].to(device)
                image1 = image1[np.newaxis, :]
                image2 = image2[np.newaxis, :]
                image3 = image3[np.newaxis, :]
                if use_random_center_crops:
                    image1 = only_resize(image1)
                    image2 = only_resize(image2)
                    image3 = only_resize(image3)
                label = dataset[index][1]
                embedding1 = model.get_embedding(image1).data.cpu().numpy()
                embedding2 = model.get_embedding(image2).data.cpu().numpy()
                embedding3 = model.get_embedding(image3).data.cpu().numpy()

                all_distances = []
                for class_num in range(0, len(class_prototypes)):
                    distance1 = np.power(embedding1 - class_prototypes[class_num], 2).sum(1)
                    distance2 = np.power(embedding2 - class_prototypes[class_num], 2).sum(1)
                    distance3 = np.power(embedding3 - class_prototypes[class_num], 2).sum(1)
                    distance = distance1 + distance2 + distance3
                    all_distances.append([model_idx_to_class_label[class_num], distance[0]])

                all_distances.sort(key=lambda x: x[1])

                if idx_to_class_label[label] in all_distances[0]:
                    correct += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:5]):
                    correct_top5 += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:10]):
                    correct_top10 += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:25]):
                    correct_top25 += 1

                total += 1
                if i >= 120:
                    break

    print(f"Top1 Accuracy: {correct / total * 100:.2f} %")
    print(f"Top5 Accuracy: {correct_top5 / total * 100:.2f} %")
    print(f"Top10 Accuracy: {correct_top10 / total * 100:.2f} %")
    print(f"Top25 Accuracy: {correct_top25 / total * 100:.2f} %")


def calculate_single_frame_accuracy_for_limited_no_samples(dataset, label_to_indices,
                                                           idx_to_class_label, model_idx_to_class_label, class_prototypes,
                                                           use_random_center_crops, img_size, model, device):
    """
    Calculate single-frame (1 image per batch) accuracy for limited batches (120 batches) per each class
    (short test accuracy calculation for quick verification)
    Logs Top1, Top5, Top10 and Top25 accuracies

    :param dataset: Test dataset
    :param label_to_indices: Dictionary that maps classes in the test dataset to corresponding indices of test samples
    :param idx_to_class_label: Dictionary that maps class ids to class labels
    :param model_idx_to_class_label: Dictionary that maps class ids to class labels - for the trained model
    :param class_prototypes: Dictionary containing class ids as keys and class center vector prototypes as values
    :param use_random_center_crops: Use random center crops during augmentation - influences resizing
    :param img_size: Size of the input image for model
    :param model: Trained few shot learner model
    :param device: Specifies where computations will be carried out, e.g.: "cuda:0", "cpu"
    """
    with torch.no_grad():
        correct = 0
        correct_top5 = 0
        correct_top10 = 0
        correct_top25 = 0
        total = 0
        only_resize = transforms.Resize(size=[img_size, img_size])
        model.eval()
        for class_id in tqdm(range(0, len(label_to_indices))):
            if len(label_to_indices[class_id]) == 0:
                continue
            lower_index, upper_index = label_to_indices[class_id][0], label_to_indices[class_id][1]
            for i, index in enumerate(range(lower_index, upper_index)):
                index = np.random.choice(range(lower_index, upper_index))
                image = dataset[index][0].to(device)
                image = image[np.newaxis, :]
                if use_random_center_crops:
                    image = only_resize(image)

                label = dataset[index][1]
                embedding = model.get_embedding(image).data.cpu().numpy()

                all_distances = []
                for class_num in range(0, len(class_prototypes)):
                    distance = np.power(embedding - class_prototypes[class_num], 2).sum(1)
                    all_distances.append([model_idx_to_class_label[class_num], distance[0]])

                all_distances.sort(key=lambda x: x[1])

                if idx_to_class_label[label] in all_distances[0]:
                    correct += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:5]):
                    correct_top5 += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:10]):
                    correct_top10 += 1
                if any(idx_to_class_label[label] in elem for elem in all_distances[:25]):
                    correct_top25 += 1

                total += 1
                if i >= 120:
                    break

    print(f"Top1 Accuracy: {correct / total * 100:.2f} %")
    print(f"Top5 Accuracy: {correct_top5 / total * 100:.2f} %")
    print(f"Top10 Accuracy: {correct_top10 / total * 100:.2f} %")
    print(f"Top25 Accuracy: {correct_top25 / total * 100:.2f} %")


def calculate_accuracy(dataloader, idx_to_class_label, model_idx_to_class_label, class_prototypes, model, device):
    """
    Calculates accuracy
    Logs Top1, Top5, Top10 and Top25 accuracies

    :param dataloader: Input data loader
    :param idx_to_class_label: Dictionary that maps class ids to class labels - for the test dataset
    :param model_idx_to_class_label: Dictionary that maps class ids to class labels - for the trained model
    :param class_prototypes: Dictionary containing class ids as keys and class center vector prototypes as values
    :param model: Trained few shot learner model
    :param device: Specifies where computations will be carried out, e.g.: "cuda:0", "cpu"
    """
    with torch.no_grad():
        correct = 0
        correct_top5 = 0
        correct_top10 = 0
        correct_top25 = 0
        correct_top50 = 0
        total = 0
        model.eval()
        for img, label in tqdm(dataloader):
            img = img.to(device)
            embedding = model.get_embedding(img).data.cpu().numpy()
            for image_id in range(0, img.shape[0]):
                img_label = label[image_id].item()
                all_distances = []
                for class_num in range(0, len(class_prototypes)):
                    distance = np.power(embedding[image_id] - class_prototypes[class_num], 2).sum(1)
                    all_distances.append([model_idx_to_class_label[class_num], distance[0]])

                all_distances.sort(key=lambda x: x[1])
                if idx_to_class_label[img_label] in all_distances[0]:
                    correct += 1
                if any(idx_to_class_label[img_label] in elem for elem in all_distances[:5]):
                    correct_top5 += 1
                if any(idx_to_class_label[img_label] in elem for elem in all_distances[:10]):
                    correct_top10 += 1
                if any(idx_to_class_label[img_label] in elem for elem in all_distances[:25]):
                    correct_top25 += 1
                if any(idx_to_class_label[img_label] in elem for elem in all_distances[:50]):
                    correct_top50 += 1
                else:
                    pass
                    # plt.imshow(img1[image_id].detach().cpu().permute(1, 2, 0))
                    # print(label1[image_id].item(), closest_class_label_id)
                    # plt.show()

                # print(f"Input image label: {label1[image_id].item()} --> Prediction {closest_class_label_id}, "
                #       f"with distance: {min_distance}")

                total += 1

    print(f"Top1 Accuracy: {correct / total * 100:.2f} %")
    print(f"Top5 Accuracy: {correct_top5 / total * 100:.2f} %")
    print(f"Top10 Accuracy: {correct_top10 / total * 100:.2f} %")
    print(f"Top25 Accuracy: {correct_top25 / total * 100:.2f} %")
    print(f"Top50 Accuracy: {correct_top50 / total * 100:.2f} %")


def inference(model, class_prototypes, file_path, idx_to_class_label, model_idx_to_class_label,
              input_image_size, device, show_plot=True, only_show_wrong_predictions=False):
    """
    Run inference with an input image from the train/test dataset
    Label of the input image is inferred from the folder structure required for training and testing datasets

    :param model: Trained few shot learner model
    :param class_prototypes: Dictionary containing class ids as keys and class center vector prototypes as values
    :param file_path: Path to input image file
    :param idx_to_class_label: Dictionary that maps class ids to class labels - for the test dataset
    :param model_idx_to_class_label: Dictionary that maps class ids to class labels - for the trained model
    :param input_image_size: Size of the input image (specified by trained model)
    :param device: Device to run the calculations on, e.g.: "cuda:0", "cpu"
    :param show_plot: If True, displays the input image as plot
    :param only_show_wrong_predictions: If True, displays only the wrongly predicted input images as plot
    :return:
    """
    with torch.no_grad():
        model.eval()
        transform = transforms.Compose([
            SquarePad(),
            transforms.Resize(size=[input_image_size, input_image_size]),  # resize image
            transforms.ToTensor(),  # transform to tensor and rescale to the range [0, 1]
            # transforms.RandomErasing(p=1, scale=(0.02, 0.5), ratio=(0.3, 3.3))  # just experimentation with occlusion
        ])

        img_head, img_tail = os.path.split(file_path)
        class_label = os.path.basename(os.path.normpath(img_head))

        image = Image.open(file_path).convert('RGB')
        image = transform(image)
        image = image.unsqueeze_(0)
        image = image.to(device)

        embedding = model.get_embedding(image).data.cpu().numpy()

        all_distances = []

        for class_num in range(0, len(class_prototypes)):
            distance = np.power(embedding - class_prototypes[class_num], 2).sum(1)

            all_distances.append([model_idx_to_class_label[class_num], distance[0]])
        all_distances.sort(key=lambda x: x[1])

        is_correct = "CORRECT" if any(class_label in elem for elem in all_distances[:25]) else "INCORRECT"
        print(f"\n{is_correct}: "
              f"Input image: {os.path.basename(file_path)}, "
              f"annotation: {class_label}, "
              f"prediction: {all_distances[0]}")
        print("Top 25 predictions with distances.", all_distances[:25])

        if (show_plot and not only_show_wrong_predictions) or \
                (class_label not in all_distances[0] and only_show_wrong_predictions):
            plt.imshow(image.squeeze(0).detach().cpu().permute(1, 2, 0))
            plt.show()

        return is_correct


def main():
    # Argument parsing
    parser = argparse.ArgumentParser(description='Model training and evaluation')
    parser.add_argument('--mode', action="store", type=str, required=True,
                        help="Method of running the script: 'train', 'evaluate', 'inference'")
    parser.add_argument('--batch_size', action="store", type=int, required=False, default=32,
                        help="Size of the batch used by the data loader when loading images")
    parser.add_argument('--model_checkpoint', action="store", type=str, required=False,
                        help="Saved model checkpoint to load: Either its epoch number, e.g.: '10' or 'final'.")
    parser.add_argument('--epochs', action="store", type=int, required=False, default=40,
                        help="Number of epochs to run the training for")
    parser.add_argument('--lr', action="store", type=float, required=False, default=1e-3,
                        help="Learning rate of the model")
    parser.add_argument('--force_cpu', action="store_true",
                        help="Forces target device to be CPU.")
    parser.add_argument('--cuda_id', action="store", type=int, required=False, default=0,
                        help="ID of the GPU device")
    parser.add_argument('--dataset_path', default="../datasets/alcohol_fsl_dataset/", action="store",
                        type=str, required=False, help="Relative path to the directory with the images.")
    parser.add_argument('--plot_embeddings', action="store_true",
                        help="Calculates and plots test image embeddings onto a 2D map")
    parser.add_argument('--input_size', action="store", type=int, required=False, default=128,
                        help="Size of the input images")
    parser.add_argument('--latent_size', action="store", type=int, required=False, default=128,
                        help="Dimension of the latent vectors")
    parser.add_argument('--triplet_loss_margin', action="store", type=float, required=False, default=1,
                        help="Value of the margin used with triplet loss")
    parser.add_argument('--switch_epochs_num', action="store", type=int, required=False, default=40,
                        help="Epoch number at which the loss function changes to hard-example mining")
    parser.add_argument('--input_image_path', action="store", type=str, required=False,
                        help="Path to input image file or folder")
    parser.add_argument('--show_plot', action="store_true",
                        help="Show input images during inference")
    parser.add_argument('--only_show_wrong_predictions', action="store_true",
                        help="Only show the incorrect predictions during inference")
    parser.add_argument('--num_workers', default=2, action="store",
                        type=int, required=False, help="Number of workers of the data loader")
    parser.add_argument('--pin_memory', action="store_true",
                        help="If True, the data loader will copy Tensors into CUDA pinned memory before returning them")

    args = parser.parse_args()

    input_image_size = args.input_size
    latent_vector_size = args.latent_size
    triplet_loss_margin = args.triplet_loss_margin
    no_epochs_to_switch_loss_fn = args.switch_epochs_num
    device = torch.device(f"cuda:{args.cuda_id}" if torch.cuda.is_available() and not args.force_cpu else 'cpu')

    # enables the cudNN autotuner which will benchmark a number of different ways of
    # computing convolutions in cudNN and then use the fastest method from then on
    torch.backends.cudnn.benchmark = True

    print(f"Input image size: {[input_image_size, input_image_size]}")
    print(f"Latent vector size: {latent_vector_size}")
    print(f"Triplet loss margin value: {triplet_loss_margin}")
    print(f"Epoch number when hard-example mining starts: {no_epochs_to_switch_loss_fn}")
    print(f"Batch size : {args.batch_size}")
    print(f"Dataset used: {args.dataset_path}")
    print(f"Dataloader pin memory: {args.pin_memory}")
    print(f"Dataloader number of workers: {args.num_workers}")
    if device.type != 'cpu':
        print(f"Available device: {device.type}, ID: {args.cuda_id}")
    else:
        print(f"Available device: {device.type}")

    use_random_center_crops = True

    # Create output folders
    os.makedirs('../saved_models', exist_ok=True)
    os.makedirs('../output_images', exist_ok=True)
    os.makedirs('../saved_plots', exist_ok=True)

    if not os.path.exists(os.path.join(args.dataset_path, "train")):
        print("!!!! Without train dataset, the specified dataset folder will be randomly "
              "divided into train and test datasets with a split of 20%-80%. !!!!")
        dataset_split_ratio = 0.2
        print(f"Splitting full dataset into train and test with a ratio of {dataset_split_ratio}")
        full_dataset, class_number = load_dataset_from_folder(args.dataset_path, input_image_size,
                                                              use_random_center_crops)
        # Splitting the dataset into training test sets
        train_data_len = int(len(full_dataset) * dataset_split_ratio)
        test_data_len = int(len(full_dataset) - train_data_len)

        # Setting deterministic random_split for reproducibility - same random train-val-test split every time
        indices = torch.randperm(sum([train_data_len, test_data_len]), generator=torch.Generator().manual_seed(42)).tolist()
        train_dataset, test_dataset = [
            CustomSubset(full_dataset, indices[offset - length: offset]) for offset, length in
            zip(torch._utils._accumulate([train_data_len, test_data_len]), [train_data_len, test_data_len])
        ]
        train_dataset_augment = CustomDataset(train_dataset, to_augment=True, original_image_size=input_image_size,
                                              use_random_center_crops=use_random_center_crops)
        train_dataset_no_augment = CustomDataset(train_dataset, to_augment=False, original_image_size=input_image_size,
                                                 use_random_center_crops=use_random_center_crops)
        test_dataset_ = CustomDataset(test_dataset, to_augment=False, original_image_size=input_image_size,
                                      use_random_center_crops=use_random_center_crops)

        print(f"Train length: {len(train_dataset_augment)}")
        print(f"Test length: {len(test_dataset_)}")

    elif not os.path.exists(os.path.join(args.dataset_path, "test")):
        print("!!!! Without test dataset, the train dataset will be used for both training and testing. !!!!")
        print('Training dataset:')
        train_dataset, class_number = load_dataset_from_folder(os.path.join(args.dataset_path, "train"),
                                                               input_image_size, use_random_center_crops)
        train_dataset_augment = CustomDataset(train_dataset, to_augment=True, original_image_size=input_image_size,
                                              use_random_center_crops=use_random_center_crops)
        train_dataset_no_augment = CustomDataset(train_dataset, to_augment=False, original_image_size=input_image_size,
                                                 use_random_center_crops=use_random_center_crops)

        print('Test dataset:')
        test_dataset, _ = load_dataset_from_folder(os.path.join(args.dataset_path, "train"),
                                                   input_image_size, use_random_center_crops)
        test_dataset_ = CustomDataset(test_dataset, to_augment=False, original_image_size=input_image_size,
                                      use_random_center_crops=use_random_center_crops)

    else:
        print('Training dataset:')
        train_dataset, class_number = load_dataset_from_folder(os.path.join(args.dataset_path, "train"),
                                                               input_image_size, use_random_center_crops)
        train_dataset_augment = CustomDataset(train_dataset, to_augment=True, original_image_size=input_image_size,
                                              use_random_center_crops=use_random_center_crops)
        train_dataset_no_augment = CustomDataset(train_dataset, to_augment=False, original_image_size=input_image_size,
                                                 use_random_center_crops=use_random_center_crops)

        print('Test dataset:')
        test_dataset, _ = load_dataset_from_folder(os.path.join(args.dataset_path, "test"),
                                                   input_image_size, use_random_center_crops)
        test_dataset_ = CustomDataset(test_dataset, to_augment=False, original_image_size=input_image_size,
                                      use_random_center_crops=use_random_center_crops)

    # In case the number of test and train dataset classes differ - map the test labels to train labels
    test_class_idx_to_train_class_idx = test_dataset_.idx_to_class_label.copy()
    for k, v in test_class_idx_to_train_class_idx.items():
        test_class_idx_to_train_class_idx[k] = train_dataset.class_to_idx[v]

    colors_for_plotting = get_color_map(class_number)
    train_data_loader = DataLoader(train_dataset_augment, batch_size=args.batch_size,
                                   shuffle=True, num_workers=args.num_workers, pin_memory=args.pin_memory)
    train_data_loader_no_augment = DataLoader(train_dataset_no_augment, batch_size=args.batch_size, shuffle=True,
                                              num_workers=args.num_workers, pin_memory=args.pin_memory)
    test_data_loader = DataLoader(test_dataset_, batch_size=args.batch_size,
                                  shuffle=True, num_workers=args.num_workers, pin_memory=args.pin_memory)

    if args.mode == "inference":
        print("The mode chosen: Inference")
        if args.model_checkpoint is None:
            raise ValueError("To use the model for inference, a previous model checkpoint "
                             "must be loaded by using the '--model_checkpoint' argument")
        if args.input_image_path is None:
            raise ValueError("To use the model for inference, a path to an input image or folder is required.")

        model, model_details, _ = load_checkpoint_and_models(device, args, mode="eval")

        class_prototypes = model_details["class_prototypes"]

        if os.path.isdir(args.input_image_path):
            it_is_a_folder = True
            paths = [os.path.join(args.input_image_path, f) for f in os.listdir(args.input_image_path)]
            if all(os.path.isdir(path) for path in paths):
                paths_random = []
                for i in range(100):
                    random_class = random.choice(paths)
                    all_image_paths = [os.path.join(random_class, f) for f in os.listdir(random_class)]
                    random_image = random.choice(all_image_paths)
                    paths_random.append(random_image)
                paths = paths_random
        else:
            it_is_a_folder = False
            paths = [args.input_image_path]

        counter = 0
        for file_path in paths:
            result = inference(model, class_prototypes, file_path, test_dataset_.idx_to_class_label,
                               model_details["idx_to_class_label"], model_details["input_image_size"],
                               device, args.show_plot, args.only_show_wrong_predictions)
            if result == "CORRECT":
                counter += 1
        if it_is_a_folder:
            print(f"Good/All: {counter}/{len(paths)}")

        exit()

    if args.mode == "evaluate":
        print("The mode chosen: Evaluation")
        if args.model_checkpoint is None:
            raise ValueError("To evaluate the model, a previous model checkpoint "
                             "must be loaded by using the '--model_checkpoint' argument")

        model, model_details, _ = load_checkpoint_and_models(device, args, mode="eval")

        class_prototypes = model_details["class_prototypes"]

        if args.plot_embeddings:
            # print("Calculating embeddings for training images...")
            # train_embeddings_tl, train_plot_images, train_labels_tl = extract_embeddings(
            #     train_data_loader_no_augment,
            #     model, device,
            #     latent_vector_size,
            #     input_image_size)
            # plot_embeddings(train_plot_images, train_embeddings_tl, train_labels_tl,
            #                 list(train_data_loader.dataset.dataset.class_to_idx.keys()),
            #                 colors_for_plotting, class_number, train_dataset_augment.idx_to_class_label, mode="train")
            # print("Done.")
            print("Calculating embeddings for test images...")
            val_embeddings_tl, val_plot_images, val_labels_tl = extract_embeddings(test_data_loader,
                                                                                   model, device,
                                                                                   latent_vector_size, input_image_size)
            print("Done.")
            print("Projecting calculated embeddings onto 2D scatter-plot ...")
            plot_embeddings(val_plot_images, val_embeddings_tl, val_labels_tl,
                            list(test_data_loader.dataset.dataset.class_to_idx.keys()),
                            colors_for_plotting, class_number, test_dataset_.idx_to_class_label,
                            mode="test", plot_images=False)
            print("Done.")
            # plt.show()

        # Calculate accuracy
        # print("Calculating single-frame accuracy for 40 batches (40 images) per class ...")
        # calculate_single_frame_accuracy_for_limited_no_samples(test_dataset,
        #                                                        test_dataset_.label_to_indices,
        #                                                        test_dataset_.idx_to_class_label,
        #                                                        model_details["idx_to_class_label"],
        #                                                        class_prototypes,
        #                                                        use_random_center_crops,
        #                                                        input_image_size,
        #                                                        model, device)
        # # print("Calculating multi-frame (3) accuracy for 40 batches (40 x 3 images) per class ...")
        # calculate_multi_frame_accuracy_for_limited_no_samples(test_dataset,
        #                                                       test_dataset_.label_to_indices,
        #                                                       test_dataset_.idx_to_class_label,
        #                                                       model_details["idx_to_class_label"],
        #                                                       class_prototypes,
        #                                                       use_random_center_crops,
        #                                                       input_image_size,
        #                                                       model, device)
        # # print("Calculating test accuracy for 10 images per class...")
        # calculate_accuracy_max_10_per_class(test_dataset, test_dataset_.label_to_indices,
        #                                     test_dataset_.idx_to_class_label, model_details["idx_to_class_label"],
        #                                     class_prototypes, use_random_center_crops, input_image_size, model, device)
        print("Calculating test accuracy...")
        calculate_accuracy(test_data_loader, test_dataset_.idx_to_class_label, model_details["idx_to_class_label"],
                           class_prototypes, model, device)
        print("Done.")
        exit()

    if args.mode == "train":
        print("The mode chosen: Training")

        triplet_loss_fn = TripletLossSemiHardMining(margin=triplet_loss_margin)
        n_epochs = args.epochs

        epoch_start_num = 1
        if args.model_checkpoint is not None:
            model, model_details, epoch_start_num, optimizer = load_checkpoint_and_models(device, args, mode="train")
            if epoch_start_num >= n_epochs + 1:
                n_epochs = epoch_start_num
            if epoch_start_num >= no_epochs_to_switch_loss_fn:
                triplet_loss_fn = TripletLossHardMining(margin=triplet_loss_margin)

        else:
            embedding_net = EmbeddingNet(input_image_size, latent_vector_size)
            model = TripletNet(embedding_net)
            model.to(device)
            optimizer = optim.Adam(model.parameters(), lr=args.lr)

        for epoch in range(epoch_start_num, n_epochs + 1):
            if epoch == no_epochs_to_switch_loss_fn:
                triplet_loss_fn = TripletLossHardMining(margin=triplet_loss_margin)
            # Training
            model.train()
            triplet_train_loss = 0
            for (img, label) in tqdm(train_data_loader):
                img = img.to(device, dtype=torch.float32)

                # plt.imshow(img[0].detach().cpu().permute(1, 2, 0))
                # print(label[0])
                # plt.show()

                embedding = model(img)

                triplet_loss = triplet_loss_fn(embedding, label)

                # Backpropagation and optimizer update
                optimizer.zero_grad()
                triplet_loss.backward()
                optimizer.step()

                triplet_train_loss += triplet_loss.item()

            if not epoch % 1:
                # Evaluation
                model.eval()
                triplet_test_loss = 0
                with torch.no_grad():
                    for (img, label) in tqdm(test_data_loader):
                        if len(test_dataset.classes) != len(train_dataset.classes):
                            label = torch.tensor(np.vectorize(test_class_idx_to_train_class_idx.get)(label))
                        img = img.to(device, dtype=torch.float32)

                        embedding = model(img)

                        triplet_loss = triplet_loss_fn(embedding, label)

                        triplet_test_loss += triplet_loss.item()

                print(f"\nEpoch: {epoch}, "
                      f"Triplet train loss: {triplet_train_loss / len(train_data_loader)}, "
                      f"Triplet val loss: {triplet_test_loss / len(test_data_loader)}")
            else:
                print(f"\nEpoch: {epoch}, Triplet train loss: {triplet_train_loss / len(train_data_loader)}")

            # if epoch >= 100:
            if not epoch % 10:
                class_prototypes = calculate_class_prototypes(dataset=train_dataset,
                                                              label_to_indices=train_dataset_augment.label_to_indices,
                                                              model=model, device=device, img_size=input_image_size,
                                                              use_random_center_crops=use_random_center_crops)
                save_checkpoint(epoch, model, class_number, input_image_size,
                                latent_vector_size, class_prototypes,
                                train_dataset_augment.idx_to_class_label, optimizer, final=False)


if __name__ == "__main__":
    main()
