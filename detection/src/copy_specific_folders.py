import os
import shutil

os.makedirs("../datasets/green_screen_train_data_only_match", exist_ok=True)

txt_file = open("../datasets/1600classes_green_screen_30max/test/dirs.txt", "r")
folder_names = txt_file.read()

subfolders = [f.path for f in os.scandir("../datasets/1600classes_green_screen_30max/train") if f.is_dir()]

for folder in subfolders:
    class_label = os.path.basename(os.path.normpath(folder))
    if class_label in folder_names:
        shutil.copytree(folder, f"../datasets/green_screen_train_data_only_match/{class_label}")
