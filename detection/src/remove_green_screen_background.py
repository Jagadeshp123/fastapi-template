import cv2
import numpy as np
from colorthief import ColorThief
from PIL import Image
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d


# 900d2f02e6cf2dffa1332f8fc0bd27ad_selfie_video
# b89d95785d81054772ab11946a7c5bb7_selfie_video
# fdc734bfadc85af8ee886358bc52f630_selfie_video
# 26b602101f9defab7bc22b4908ee3823_selfie_video
video = cv2.VideoCapture(
    "../vw_videos/900d2f02e6cf2dffa1332f8fc0bd27ad_selfie_video.mp4")


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def contour_area(contours):
    cnt_area = []
    for i in range(0, len(contours), 1):
        cnt_area.append(cv2.contourArea(contours[i]))

    list.sort(cnt_area, reverse=True)
    return cnt_area


def onMouse(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print('x = %d, y = %d' % (x, y))
        print(frame[y, x, 0], frame[y, x, 1], frame[y, x, 2])
        cv2.waitKey()


threshold_gray = 200

while True:

    ret, frame = video.read()
    if frame is None:
        break

    frame = cv2.resize(frame, (360, 640))
    frame = cv2.rotate(frame, cv2.ROTATE_180)
    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    green_colors = (np.array(rgb_frame[0, 0, :]).astype('int32') +
                    np.array(rgb_frame[0, rgb_frame.shape[1] - 1, :]).astype('int32') +
                    np.array(rgb_frame[rgb_frame.shape[1] - 1, 0, :]).astype('int32') +
                    np.array(rgb_frame[rgb_frame.shape[0] - 1, rgb_frame.shape[1] - 1, :].astype('int32'))) / 4

    distance_from_gray = np.zeros((rgb_frame.shape[0], rgb_frame.shape[1]))

    for y in range(0, rgb_frame.shape[0]):
        for x in range(0, rgb_frame.shape[1]):
            difference = rgb_frame[y, x] - green_colors
            distance_from_gray[y, x] = np.abs(difference[0] + difference[1] + difference[2])

    columns = distance_from_gray.sum(axis=0)
    rows = distance_from_gray.sum(axis=1)

    columns *= (rgb_frame.shape[0]/np.amax(columns))
    rows *= (rgb_frame.shape[1]/np.amax(rows))
    columns_ = gaussian_filter1d(columns, 10)
    rows_ = gaussian_filter1d(rows, 10)
    fig, ax = plt.subplots()
    ax.imshow(rgb_frame, extent=[0, rgb_frame.shape[1], 0, rgb_frame.shape[0]])
    ax.plot(np.arange(rgb_frame.shape[1]), columns_)
    ax.plot(rows_, (-np.arange(rgb_frame.shape[0])) + rgb_frame.shape[0])
    # ax.plot(np.arange(rgb_frame.shape[1]), columns)
    # ax.plot(rows, (-np.arange(rgb_frame.shape[0])) + rgb_frame.shape[0])

    max_row = np.argmax(rows_)
    max_column = np.argmax(columns_)
    ax.plot(max_column, columns_[max_column], 'ro')
    ax.plot(rows_[max_row], -max_row + rgb_frame.shape[0], 'ro')
    intersection = line_intersection(([max_column, 0], [max_column, rgb_frame.shape[0]]),
                                     ([0, -max_row + rgb_frame.shape[0]], [rgb_frame.shape[1], -max_row + rgb_frame.shape[0]]))
    ax.plot(intersection[0], intersection[1], 'ro')

    plt.show()

    # frame = cv2.medianBlur(frame, 15)
    gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

    gray = cv2.medianBlur(gray, 5)
    gray[gray < threshold_gray] = 0
    gray[gray >= threshold_gray] = 1

    hsv = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)

    h, s, v = cv2.split(hsv)
    v[v < 250] = 0
    final_hsv = cv2.merge((h, s, v))
    res = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2RGB)

    # masked_result = frame.copy()
    # masked_result[v == 0] = 0

    contours, _ = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnt_area = contour_area(contours)
    for c in contours:
        if cv2.contourArea(c) >= cnt_area[0]:
            rect = cv2.minAreaRect(c)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            rect = cv2.boundingRect(c)
            x, y, w, h = rect

            width = int(w)
            height = int(h)
            src_pts = box.astype("float32")
            dst_pts = np.array([[0, height - 1],
                                [0, 0],
                                [width - 1, 0],
                                [width - 1, height - 1]], dtype="float32")
            M = cv2.getPerspectiveTransform(src_pts, dst_pts)
            warped = cv2.warpPerspective(frame, M, (width, height))
            crop_img = frame.copy()[y:y + h, x:x + w]

            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.imshow("cropped", crop_img)
    cv2.imshow("thresholded", res)
    cv2.imshow("video", frame)
    cv2.setMouseCallback('video', onMouse)

    cv2.imshow("gray", gray * 255)

    if cv2.waitKey(1) == 27:
        break

video.release()
cv2.destroyAllWindows()
