import os
import shutil
import os.path
import argparse
import random
import string
from tqdm import tqdm
from glob import glob

if __name__ == '__main__':
    # Argument parsing
    parser = argparse.ArgumentParser(description='File copying')
    parser.add_argument('--source_path', action="store", type=str, required=True,
                        help="Path to the source dataset folder")
    parser.add_argument('--destination_path', action="store", type=str, required=True,
                        help="Path to the destination dataset folder")
    parser.add_argument('--max_files', action="store", type=int, required=False, default=20,
                        help="Max. number of files to copy")

    args = parser.parse_args()
    dir_to_copy_images_from = args.source_path
    dir_to_copy_images_to = args.destination_path

    subfolders = [f.name for f in os.scandir(dir_to_copy_images_from) if f.is_dir()]

    os.makedirs(dir_to_copy_images_to, exist_ok=True)

    for subfolder in tqdm(subfolders):
        os.makedirs(os.path.join(dir_to_copy_images_to, subfolder), exist_ok=True)
        all_files = [y for x in os.walk(os.path.join(dir_to_copy_images_from, subfolder)) for y in glob(os.path.join(x[0], '*.png'))]
        random.shuffle(all_files)
        no_random_files_to_choose = min(args.max_files, len(all_files))
        for i, file in enumerate(all_files):
            if i == no_random_files_to_choose:
                break
            _, filename = os.path.split(file)
            if os.path.exists(os.path.join(dir_to_copy_images_to, subfolder, filename)):
                generated_random_string = ''.join(random.choices(string.ascii_lowercase, k=10))
                renamed_file_path = f"{file[:-4]}_{generated_random_string}{file[-4:]}"
                os.rename(file, renamed_file_path)
                shutil.copy2(renamed_file_path, os.path.join(dir_to_copy_images_to, subfolder))
            else:
                shutil.copy2(file, os.path.join(dir_to_copy_images_to, subfolder))