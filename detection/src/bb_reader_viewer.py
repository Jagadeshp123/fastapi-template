import os
import torch
import argparse
import numpy as np
from PIL import Image
from pathlib import Path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from tqdm import tqdm


def main():
    # Argument parsing
    parser = argparse.ArgumentParser(description='Bounding box cropping and visualizing')
    parser.add_argument('--input', action="store", type=str, required=False,
                        help="Path to input image file or folder")
    parser.add_argument('--target_path', action="store", type=str, required=False,
                        help="Path to store the cropped bounding box image regions")

    args = parser.parse_args()

    print("Reading files from folders...")
    if os.path.isdir(args.input):
        img_paths = []
        annotation_paths = []
        for root, _, filenames in sorted(os.walk(args.input, followlinks=True)):
            for filename in sorted(filenames):
                file_path = os.path.join(root, filename)
                if file_path.endswith(".png") or file_path.endswith(".jpg"):
                    img_paths.append(file_path)
                if file_path.endswith(".txt"):
                    annotation_paths.append(file_path)
    else:
        img_paths = [args.input]
        pre, ext = os.path.splitext(args.input)
        annotation_paths = [f"{pre}.txt"]

    print("Done.")

    # visualize_bbs(img_paths, annotation_paths)

    target_path = args.target_path
    if target_path is None:
        target_path = "../fsl_images"
    os.makedirs(target_path, exist_ok=True)

    crop_and_save_bb_regions(img_paths, annotation_paths, target_path)


def visualize_bbs(img_paths, annotation_paths):
    """
    Visualize bounding boxes on input images

    :param img_paths: List of input image paths
    :param annotation_paths: List of corresponding input image annotations
    """
    for i, img_path in enumerate(img_paths):
        image = Image.open(img_path).convert('RGB')
        with open(annotation_paths[i]) as f:
            annotation = f.readlines()
            x_min, y_min, x_max, y_max = annotation[0].split()
            width = int(x_max) - int(x_min)
            height = int(y_max) - int(y_min)
        rect = patches.Rectangle((int(x_min), int(y_min)), width, height, linewidth=1, edgecolor='r', facecolor='none')
        plt.imshow(image)
        ax = plt.gca()  # Get the current reference
        ax.add_patch(rect)
        plt.show()


def crop_and_save_bb_regions(img_paths, annotation_paths, target_path):
    """
    Crop bounding boxes from images and saved the cropped areas as separate files

    :param img_paths: List of input image paths
    :param annotation_paths: List of corresponding input image annotations
    :param target_path: Path to the folder where the cropped images will be saved
    """

    print(f"Cropping objects from images and saving them to {target_path}...")
    skipped_imgs = []
    for i in tqdm(range(0, len(img_paths))):
        img_head, img_tail = os.path.split(img_paths[i])
        image = Image.open(img_paths[i]).convert('RGB')
        img_width, img_height = image.size

        with open(annotation_paths[i]) as f:
            annotation = f.readlines()
            label, x_min, y_min, x_max, y_max, label2 = annotation[0].split()
            os.makedirs(f"{target_path}/{label}", exist_ok=True)

            label, x_min, y_min, x_max, y_max = str(label), int(x_min), int(y_min), int(x_max), int(y_max)

            if y_min > y_max or x_min > x_max:
                skipped_imgs.append(img_paths[i])
                print(f"BB min-max error for image: {img_paths[i]}")
                print(f"Bounding box coordinates: x: ({(x_min, x_max)}), y: ({(y_min, y_max)})")
                print(f"Image width and height: {img_width, img_height}")
            cropped_object = image.crop((max(x_min, 0), max(y_min, 0),
                                        min(x_max, img_width), min(y_max, img_height)))
            cropped_img_filename = img_tail[:-4] + f"_cropped.png"
            try:
                cropped_img_path = f"{target_path}/{label}/{cropped_img_filename}"
                if not os.path.exists(cropped_img_path):
                    cropped_object.save(cropped_img_path)
            except SystemError:
                skipped_imgs.append(img_paths[i])
                print(f"SystemError: {img_paths[i]}")
                print(f"Bounding box coordinates: "
                      f"x: ({(max(x_min, 0), min(x_max, img_width))}), y: ({(max(y_min, 0), min(y_max, img_height))})")
                print(f"Image width:height: {img_width, img_height}")
    print("Done.")
    print(f"Number of skipped images: {len(skipped_imgs)}"
          f"\n{skipped_imgs}")


if __name__ == "__main__":
    main()

