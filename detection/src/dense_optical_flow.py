import os
import csv
import cv2
import math
import shutil
# import time
# import imutils
# import argparse
import numpy as np
from tqdm import tqdm
# from PIL import Image
from detection.src import few_shot_learner_api
# import few_shot_learner_api
import matplotlib.pyplot as plt
from collections import Counter
# from imutils.video import FPS
# from imutils.video import VideoStream

from detection.src  import yolo_detect
from detection.src  import blur_detector


def bb_intersection_over_union(boxA, boxB):
    if boxA is None or boxB is None:
        return 0
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the intersection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou


def draw_flow(img, flow, step=16, to_gray=False):
    """
    Draw optical flow vectors onto the input image

    :param img: Image to draw the flow vectors on
    :param flow: Output of the optical flow calculation between two consecutive frames
    :param step: Sparsity of the drawn optical flow vectors on the image
    :param to_gray: If False: image is first converted to BGR image and then overlaid with the vectors
    :return: Input image overlaid with the optical flow vectors
    """
    h, w = img.shape[:2]
    y, x = np.mgrid[step / 2:h:step, step / 2:w:step].reshape(2, -1).astype(int)
    fx, fy = flow[y, x].T
    lines = np.vstack([x, y, x + fx, y + fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    if not to_gray:
        vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    else:
        vis = img
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis


def contour_area(contours):
    """
    Calculate the area of the contours and sort them in decreasing order

    :param contours: Contours on an image
    :return: List of calculated area of contours in decreasing order
    """
    cnt_area = []
    for i in range(0, len(contours), 1):
        cnt_area.append(cv2.contourArea(contours[i]))

    list.sort(cnt_area, reverse=True)
    return cnt_area


def identify_and_draw_bounding_box(contours, image1, image2, draw=True):
    """
    Draws a rotated bounding box around the largest contour in the image

    :param contours: Detected contours in the image
    :param image1: Image to draw the bounding box on
    :param image2: Image to draw the bounding box on
    :param draw: If True: draws a bounding box on the input image
    :return: image1, image2 and obtained bounding box coordinates
    """
    ret_bb = []
    axes_aligned_bb_coordinates = []
    # Call our function to get the list of contour areas
    cnt_area = contour_area(contours)

    # Loop through each contour of our image
    for i in range(0, len(contours), 1):
        cnt = contours[i]
        # Calculate the area of the contour and only draw bb with the largest area
        if cv2.contourArea(cnt) >= cnt_area[0]:
            rect = cv2.minAreaRect(cnt)  # Find the minimum area rotated rectangle around it
            x_min, y_min, width, height = cv2.boundingRect(cnt)
            x_max = x_min + width
            y_max = y_min + height
            axes_aligned_bb_coordinates = [x_min, y_min, x_max, y_max]
            box = np.int0(cv2.boxPoints(rect))   # Find the four vertices of a rotated rectangle
            if draw:
                image1 = cv2.drawContours(image1, [box], 0, (0, 0, 255), 3)
                # image2 = cv2.drawContours(image2, [box], 0, (0, 0, 255), 3)
            ret_bb.append(rect)

            # image1 = cv2.rectangle(image1, (x, y), (x + w, y + h), (0, 0, 255), 2)
            image2 = cv2.rectangle(image2, (x_min, y_min), (x_max, y_max), (0, 0, 255), 2)
            # ret_bb.append([x, y, w, h])

    return image1, image2, ret_bb, axes_aligned_bb_coordinates


def scale_coords(img1_shape, coords, img0_shape, ratio_pad=None):
    # Rescale coords (xyxy) from img1_shape to img0_shape
    if ratio_pad is None:  # calculate from img0_shape
        gain = min(img1_shape[0] / img0_shape[0], img1_shape[1] / img0_shape[1])  # gain  = old / new
        pad = (img1_shape[1] - img0_shape[1] * gain) / 2, (img1_shape[0] - img0_shape[0] * gain) / 2  # wh padding
    else:
        gain = ratio_pad[0][0]
        pad = ratio_pad[1]
    coords[0] -= pad[0]  # x padding
    coords[2] -= pad[0]  # x padding
    coords[1] -= pad[1]  # y padding
    coords[3] -= pad[1]  # y padding
    coords = np.divide(coords, gain)
    clip_coords(coords, img0_shape)
    return coords


def clip_coords(boxes, img_shape):
    # Clip bounding xyxy bounding boxes to image shape (height, width)
    boxes[0] = np.clip(boxes[0], 0, img_shape[1])  # x1
    boxes[1] = np.clip(boxes[1], 0, img_shape[0])  # y1
    boxes[2] = np.clip(boxes[2], 0, img_shape[1])  # x2
    boxes[3] = np.clip(boxes[3], 0, img_shape[0])  # y2


def split_video(video_path):
    """
    Helper function to split the video into smaller intervals
    Press 'r' to start recording and press 'r' again to stop the recording. The frames in-between the two presses
    will be saved as a separate video file
    Afterwards, the input recording is continued and the process can be repeated

    :param video_path: Input video path
    """
    # read the video
    cap = cv2.VideoCapture(video_path)
    orientation = cap.get(cv2.CAP_PROP_ORIENTATION_META)
    i = 0
    while True:
        out = cv2.VideoWriter(f"../{i}.mp4", cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (1080, 1920))
        i += 1
        status = "start"
        while True:
            ret, frame = cap.read()
            if orientation == 270.0:
                frame = cv2.rotate(frame, cv2.ROTATE_180)
            cv2.imshow("frame", frame)
            k = cv2.waitKey(30)
            if k == ord("r") and status == "start":
                while True:
                    out.write(frame)
                    k = cv2.waitKey(1)
                    if k == ord("r"):
                        status = "stop"
                        out.release()
                        break
                    else:
                        ret, frame = cap.read()
                        if orientation == 270.0:
                            frame = cv2.rotate(frame, cv2.ROTATE_180)
                        cv2.imshow("frame", frame)
                if status == "stop":
                    break
            if frame is None:
                break


def dense_optical_flow(method, video_path, params=[], to_gray=False, tvl1=False):
    """
    Processes the input video's frames one by one and calculates optical flow between the consecutive frames.
    If "b" is pressed, a bounding box is extracted from
    the current camera frame based on the current optical flow heatmap.

    :param method: Selected optical flow method, e.g.: cv2.optflow.calcOpticalFlowSparseToDense
    :param video_path: Path to the input video file
    :param params: Optional; additional parameters of the selected method
    :param to_gray: Optional; if True: converts the input video's frames to grayscale
                    (some optical flow methods require it)
    :param tvl1: Set to True if the selected method is tvl1, i.e.: cv2.optflow.DualTVL1OpticalFlow_create
    """
    resolution = (720, 1280)

    # read the video
    cap = cv2.VideoCapture(video_path)
    orientation = cap.get(cv2.CAP_PROP_ORIENTATION_META)
    # Read the first frame
    ret, old_frame = cap.read()

    # create an output video from the input frames
    out = cv2.VideoWriter('../output.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 15, (resolution[0] * 2, resolution[1]))

    old_frame = cv2.resize(old_frame, resolution)
    if orientation == 270.0:
        old_frame = cv2.rotate(old_frame, cv2.ROTATE_180)  # Needed for front-side cameras

    # Variables for the replay functionality
    all_frames = [old_frame]
    all_heatmaps = [None]
    all_magnitude_angles = [None]

    hsv = np.zeros_like(old_frame)
    hsv[..., 1] = 255

    # Preprocessing for exact method
    old_frame_colored = old_frame
    if to_gray:
        old_frame = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)

    frame_counter = 0
    while True:
        # Read the next frame
        ret, new_frame = cap.read()
        if not ret:
            break
        if new_frame is not None:
            new_frame = cv2.resize(new_frame, resolution)
        if orientation == 270.0:
            new_frame = cv2.rotate(new_frame, cv2.ROTATE_180)  # Needed for front-side cameras
        all_frames.append(new_frame)
        frame_copy = new_frame.copy()

        # Preprocessing for exact method
        new_frame_colored = new_frame
        if to_gray:
            new_frame = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
        # Calculate Optical Flow
        if tvl1:
            flow = method.calc(old_frame, new_frame, None, *params)
        else:
            flow = method(old_frame, new_frame, None, *params)

        # Encoding: convert the algorithm's output into Polar coordinates
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1], angleInDegrees=True)
        # mag = median_filter(mag, size=3, cval=0, mode='constant')

        # Use Hue and Saturation to encode the Optical Flow
        hsv[..., 0] = ang

        normalized_mag = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        hsv[..., 2] = normalized_mag
        # Convert HSV image into BGR for demo
        bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        all_magnitude_angles.append([normalized_mag, ang])

        vectors = draw_flow(frame_copy.copy(), flow, to_gray=to_gray, step=16)
        vis = np.concatenate((bgr, vectors), axis=1)
        out.write(vis)

        bgr = cv2.medianBlur(bgr, 115)
        all_heatmaps.append(bgr)

        combined_flow_img = cv2.addWeighted(vectors, 0.2, bgr, 0.8, 0)
        cv2.imshow("optical flow", combined_flow_img)

        k = cv2.waitKey(25) & 0xFF  # escape button
        if k == 27:
            out.release()
            break

        if k == ord("h"):
            plt.xlim(xmin=0, xmax=125)
            plt.hist(mag)
            plt.show()

        if k == 32:  # space button
            while True:
                k = cv2.waitKey(0)
                if k == 32:
                    break

        if k == ord("r"):  # replay video frames and optical flow results
            for frame_i in range(len(all_frames) - 1, 0, -1):
                combined_flow_img = cv2.addWeighted(all_frames[frame_i], 0.2, all_heatmaps[frame_i], 0.8, 0)
                cv2.imshow("replay", cv2.resize(combined_flow_img, (resolution[0] // 4, resolution[1] // 4)))
                k = cv2.waitKey(100)
                if k == ord("r"):
                    all_frames = [None]
                    all_heatmaps = [None]
                    all_magnitude_angles = [None]
                    break

                if k == 32:  # space button
                    while True:
                        k = cv2.waitKey(0)
                        if k == 32:
                            break

        if k == ord("b"):  # draw bounding boxes based on contours extracted from optical flow heatmap
            filtered_of_gray = cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY)
            _, thresholded_img = cv2.threshold(filtered_of_gray, 10, 255, cv2.THRESH_BINARY)
            # Generate contours based mask
            contours, hierarchy = cv2.findContours(thresholded_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            bgr, vectors, ret_bb, _ = identify_and_draw_bounding_box(contours, bgr, vectors)
            cv2.imshow("optical flow", bgr)
            cv2.imshow("frame", vectors)
            if len(ret_bb) != 0:
                (x, y), (w, h), a = ret_bb[0]
                width = int(w)
                height = int(h)
                box = cv2.boxPoints(ret_bb[0])
                box = np.int0(box)
                src_pts = box.astype("float32")
                dst_pts = np.array([[0, height - 1],
                                    [0, 0],
                                    [width - 1, 0],
                                    [width - 1, height - 1]], dtype="float32")
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                warped = cv2.warpPerspective(old_frame_colored, M, (width, height))
                warped_heatmap = cv2.warpPerspective(bgr, M, (width, height))
                warped_heatmap = cv2.cvtColor(warped_heatmap, cv2.COLOR_BGR2GRAY)
                warped_heatmap = cv2.normalize(warped_heatmap.astype('float32'), None, 0.0, 1.0, cv2.NORM_MINMAX)
                warped_heatmap = np.sqrt(warped_heatmap)
                warped_weighted = (warped.astype('float32') * np.expand_dims(warped_heatmap, axis=2)).astype('uint8')

                med_ang, median_magnitude, mean_magnitude = calc_cropped_relative_median_angle(M, ang, height, mag, width)

                med_ang = math.degrees(med_ang[0][0])
                print("Median angle of cropped: ", med_ang)
                print("Mean magnitude of cropped: ", mean_magnitude)
                print("Median magnitude of cropped: ", median_magnitude)

                keep_bb = False
                angle_filter = 160 <= med_ang <= 200 or 340 <= med_ang <= 360 or 0 <= med_ang <= 20
                mag_filter = median_magnitude >= 15
                if mag_filter or (angle_filter and median_magnitude >= 5):
                    keep_bb = True
                print(f"Keep bb?: {keep_bb}")
                # old_gray = cv2.cvtColor(warped_weighted, cv2.COLOR_BGR2GRAY)
                # feature_params = dict(maxCorners=30, qualityLevel=0.5, minDistance=7, blockSize=7)
                # p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
                # for point in p0:
                #     warped_weighted = cv2.circle(warped_weighted, (int(point[0][0]), int(point[0][1])), radius=3, color=(0, 0, 255), thickness=-1)

                # hand_cropped = None
                # if med_ang > 90 and med_ang < 270:
                #     hand_cropped = warped[:, :warped.shape[0], :]
                # else:
                #     hand_cropped = warped[:, warped.shape[1] - warped.shape[0]:, :]
                # cv2.imshow("hand_cropped", hand_cropped)
                cv2.imshow("cropped_object", warped)
                # cv2.imshow("cropped_heatmap", warped_heatmap)
                # cv2.imshow("cropped_weighted", warped_weighted)
            while True:
                k = cv2.waitKey(0)
                if k == ord("b"):  # if 'b' is pressed again, let the video continue
                    break

        old_frame = new_frame
        old_frame_colored = new_frame_colored
        frame_counter += 1
        print("Frame: ", frame_counter)

    out.release()


def dense_optical_flow_with_fsl(method, video_path, model_path, class_label=None,
                                params=[], to_gray=False, tvl1=False, use_heatmap_masking=False):
    """
    Processes the input video's frames one by one and calculates optical flow between the consecutive frames.
    If "b" is pressed, a bounding box is extracted from
    the current camera frame based on the current optical flow heatmap.
    If "r" is pressed, a replay is started from the current camera frame. The saved previous camera frames and
    optical flow is overlaid on top of each other and for each frame a bounding box is calculated. When "r" is pressed
    again (or the replay reached the 0th frame) the extracted bounding boxes are sorted based on their area and
    the median elements are chosen as candidates for object recognition.

    :param method: Selected optical flow method, e.g.: cv2.optflow.calcOpticalFlowSparseToDense
    :param video_path: Path to the input video file
    :param model_path: Path to the few shot learner model file
    :param class_label: Label of the object of interest in the input video
    :param params: Optional; additional parameters of the selected method
    :param to_gray: Optional; if True: converts the input video's frames to grayscale
                    (some optical flow methods require it)
    :param tvl1: Set to True if the selected method is tvl1, i.e.: cv2.optflow.DualTVL1OpticalFlow_create
    :param use_heatmap_masking: Optional; If True: optical flow heatmap is used as a mask to crop the object
    """
    resolution = (720, 1280)
    # read the video
    cap = cv2.VideoCapture(video_path)
    orientation = cap.get(cv2.CAP_PROP_ORIENTATION_META)
    # Read the first frame
    ret, old_frame = cap.read()

    out = cv2.VideoWriter('../output.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 15, (resolution[0] * 2, resolution[1]))

    old_frame = cv2.resize(old_frame, resolution)
    # Needed for selfie cameras, comment out with other type of videos
    if orientation == 270.0:
        old_frame = cv2.rotate(old_frame, cv2.ROTATE_180)

    all_frames = [old_frame]
    all_heatmaps = [None]
    all_magnitude_angles = [None]

    hsv = np.zeros_like(old_frame)
    hsv[..., 1] = 255

    # Preprocessing for exact method
    old_frame_colored = old_frame
    if to_gray:
        old_frame = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)

    frame_counter = 0
    while True:
        # Read the next frame
        ret, new_frame = cap.read()
        if not ret:
            break
        if new_frame is not None:
            new_frame = cv2.resize(new_frame, resolution)
        if orientation == 270.0:
            new_frame = cv2.rotate(new_frame, cv2.ROTATE_180)
        all_frames.append(new_frame)
        frame_copy = new_frame.copy()

        # Preprocessing for exact method
        new_frame_colored = new_frame
        if to_gray:
            new_frame = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
        # Calculate Optical Flow
        if tvl1:
            flow = method.calc(old_frame, new_frame, None, *params)
        else:
            flow = method(old_frame, new_frame, None, *params)

        # Encoding: convert the algorithm's output into Polar coordinates
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1], angleInDegrees=True)

        # Use Hue and Saturation to encode the Optical Flow
        hsv[..., 0] = ang

        normalized_mag = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        hsv[..., 2] = normalized_mag
        # Convert HSV image into BGR for demo
        bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        all_magnitude_angles.append([normalized_mag, ang])

        vectors = draw_flow(frame_copy.copy(), flow, to_gray=to_gray, step=16)
        # cv2.imshow("frame", vectors)
        vis = np.concatenate((bgr, vectors), axis=1)
        out.write(vis)
        # cv2.imshow("optical flow", bgr)
        bgr = cv2.medianBlur(bgr, 115)
        # blur = cv2.GaussianBlur(bgr, (5, 5), 0)
        # bgr = cv2.blur(bgr, (15, 15))
        all_heatmaps.append(bgr)

        # bgr = cv2.blur(bgr, (40, 40))
        # cv2.imshow("optical flow", bgr)
        if len(all_frames) > 500:
            all_frames = [new_frame]
            all_heatmaps = [bgr]
            all_magnitude_angles = [normalized_mag, ang]
        combined_flow_img = cv2.addWeighted(vectors, 0.2, bgr, 0.8, 0)
        cv2.imshow("optical flow", combined_flow_img)

        k = cv2.waitKey(25) & 0xFF
        if k == 27:
            out.release()
            break

        if k == ord("w"):
            while True:
                k = cv2.waitKey(0)
                if k == ord("w"):
                    break

        if k == ord("r"):
            cropped_bb_list = []
            for frame_i in range(len(all_frames) - 1, 0, -1):
                combined_flow_img = cv2.addWeighted(all_frames[frame_i], 0.2, all_heatmaps[frame_i], 0.8, 0)
                cv2.imshow("replay", cv2.resize(combined_flow_img, (resolution[0] // 4, resolution[1] // 4)))
                k = cv2.waitKey(100)
                if k == ord("r"):
                    all_frames = [None]
                    all_heatmaps = [None]
                    all_magnitude_angles = [None]
                    break
                else:
                    filtered_of_gray = cv2.cvtColor(all_heatmaps[frame_i], cv2.COLOR_BGR2GRAY)
                    _, thresholded_img = cv2.threshold(filtered_of_gray, 10, 255, cv2.THRESH_BINARY)
                    # Generate contours based mask

                    contours, hierarchy = cv2.findContours(thresholded_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                    bgr, _, ret_bb, _ = identify_and_draw_bounding_box(contours, all_heatmaps[frame_i], all_heatmaps[frame_i], draw=False)
                    if len(ret_bb) != 0:
                        (x, y), (w, h), a = ret_bb[0]
                        width = int(w)
                        height = int(h)
                        box = cv2.boxPoints(ret_bb[0])
                        box = np.int0(box)
                        src_pts = box.astype("float32")
                        dst_pts = np.array([[0, height - 1],
                                            [0, 0],
                                            [width - 1, 0],
                                            [width - 1, height - 1]], dtype="float32")
                        M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                        warped = cv2.warpPerspective(all_frames[frame_i], M, (width, height))

                        if use_heatmap_masking:
                            warped_heatmap = cv2.warpPerspective(all_heatmaps[frame_i], M, (width, height))
                            warped_heatmap = cv2.cvtColor(warped_heatmap, cv2.COLOR_BGR2GRAY)
                            warped_heatmap = cv2.normalize(warped_heatmap.astype('float32'), None, 0.0, 1.0,
                                                           cv2.NORM_MINMAX)
                            warped_heatmap = np.sqrt(warped_heatmap)
                            warped = (warped.astype('float32') * np.expand_dims(warped_heatmap, axis=2)).astype('uint8')

                        cropped_bb_list.append([cv2.rotate(warped, cv2.ROTATE_90_CLOCKWISE), frame_i, warped.shape[0] * warped.shape[1]])

                if k == 32:
                    while True:
                        k = cv2.waitKey(0)
                        if k == 32:
                            break

            cropped_bb_list.sort(key=lambda elem: elem[2])
            median_element = len(cropped_bb_list) // 2
            final_prediction_top1 = []
            final_prediction_top5 = []
            for bb_i in range(max(median_element - 3, 0), min(median_element + 3, len(cropped_bb_list))):
                cv2.imwrite(f"../cropped_bbs/{cropped_bb_list[bb_i][1]}.jpg", cropped_bb_list[bb_i][0])

                top1_prediction_class_name, all_distances = \
                    few_shot_learner_api.inference(model_path=model_path,
                                                   input_image=f"../cropped_bbs/{cropped_bb_list[bb_i][1]}.jpg",  # cv2.cvtColor(cropped_bb_list[bb_i][0], cv2.COLOR_BGR2RGB)
                                                   annotation_name=class_label,
                                                   force_cpu=True,
                                                   show_plot=False,
                                                   verbose=True)
                final_prediction_top1.append(top1_prediction_class_name)
                for elem in all_distances[:5]:
                    final_prediction_top5.append(elem[0])

            final_prediction_top1_counter = Counter(final_prediction_top1)
            final_prediction_top5_counter = Counter(final_prediction_top5)
            top1_verdict = ""
            top5_verdict = ""
            if class_label is not None:
                top1_verdict = "CORRECT" if\
                    class_label == final_prediction_top1_counter.most_common(1)[0][0] else "INCORRECT"
                top5_verdict = "CORRECT" if\
                    class_label in [label for label, count in final_prediction_top5_counter.most_common(5)] else "INCORRECT"
            print(f"Final prediction from {len(final_prediction_top1)} images:")
            print(f"based on TOP1 predictions: {top1_verdict} - {final_prediction_top1_counter.most_common(1)[0][0]}")
            print(f"based on TOP5 predictions: {top5_verdict} - {final_prediction_top5_counter.most_common(5)}")
            print(final_prediction_top5_counter)

            while True:
                k = cv2.waitKey(0)
                if k == 32:
                    break

        if k == ord("b"):
            filtered_of_gray = cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY)
            # canny_output = cv2.Canny(filtered_of, 0, 20)
            # filtered_of = filtered_of[filtered_of > 0]
            _, thresholded_img = cv2.threshold(filtered_of_gray, 10, 255, cv2.THRESH_BINARY)
            # Generate contours based mask

            contours, hierarchy = cv2.findContours(thresholded_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            bgr, vectors, ret_bb, _ = identify_and_draw_bounding_box(contours, bgr, vectors)
            cv2.imshow("optical flow", bgr)
            cv2.imshow("frame", vectors)
            if len(ret_bb) != 0:
                (x, y), (w, h), a = ret_bb[0]
                width = int(w)
                height = int(h)
                box = cv2.boxPoints(ret_bb[0])
                box = np.int0(box)
                src_pts = box.astype("float32")
                dst_pts = np.array([[0, height - 1],
                                    [0, 0],
                                    [width - 1, 0],
                                    [width - 1, height - 1]], dtype="float32")
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                warped = cv2.warpPerspective(old_frame_colored, M, (width, height))
                warped_heatmap = cv2.warpPerspective(bgr, M, (width, height))
                warped_heatmap = cv2.cvtColor(warped_heatmap, cv2.COLOR_BGR2GRAY)
                warped_heatmap = cv2.normalize(warped_heatmap.astype('float32'), None, 0.0, 1.0, cv2.NORM_MINMAX)
                warped_heatmap = np.sqrt(warped_heatmap)
                warped_weighted = (warped.astype('float32') * np.expand_dims(warped_heatmap, axis=2)).astype('uint8')

                med_ang, median_magnitude, mean_magnitude = calc_cropped_relative_median_angle(M, ang, height, mag, width)

                med_ang = math.degrees(med_ang[0][0])
                print("Median angle of cropped: ", med_ang)

                # old_gray = cv2.cvtColor(warped_weighted, cv2.COLOR_BGR2GRAY)
                # feature_params = dict(maxCorners=30, qualityLevel=0.5, minDistance=7, blockSize=7)
                # p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
                # for point in p0:
                #     warped_weighted = cv2.circle(warped_weighted, (int(point[0][0]), int(point[0][1])), radius=3, color=(0, 0, 255), thickness=-1)

                # hand_cropped = None
                # if med_ang > 90 and med_ang < 270:
                #     hand_cropped = warped[:, :warped.shape[0], :]
                # else:
                #     hand_cropped = warped[:, warped.shape[1] - warped.shape[0]:, :]
                # cv2.imshow("hand_cropped", hand_cropped)
                cv2.imshow("cropped_object", warped)
                cv2.imshow("cropped_heatmap", warped_heatmap)
                cv2.imshow("cropped_weighted", warped_weighted)
            while True:
                k = cv2.waitKey(0)
                if k == ord("b"):
                    break

        old_frame = new_frame
        old_frame_colored = new_frame_colored
        frame_counter += 1
        print("Frame: ", frame_counter)

    out.release()


def calc_cropped_relative_median_angle(M, ang, height, mag, width):
    """
    Calculate relative median angle and magnitude of optical flow vectors inside the cropped area

    :param M: Output of cv2.getPerspectiveTransform()
    :param ang: array of angles of the optical flow vectors
    :param height: height of the cropped area
    :param mag: array of magnitudes of the optical flow vectors
    :param width: width of the cropped area
    :return: median angle, median magnitude and mean magnitude of optical flow vectors
    """
    median_angle, median_magnitude, mean_magnitude = calc_median_angle(M, ang, height, mag, width)
    # median_motion_vector_cart = cv2.polarToCart(10.0, median_angle)
    # median_motion_vector_cart_3d = np.array(
    #     [median_motion_vector_cart[0][0][0], median_motion_vector_cart[1][0][0], 0.0])
    # rotated_median_motion_vector_cart_3d = np.dot(M, median_motion_vector_cart_3d)
    # _, med_ang = cv2.cartToPolar(rotated_median_motion_vector_cart_3d[0], rotated_median_motion_vector_cart_3d[1])
    return median_angle, median_magnitude, mean_magnitude


def calc_median_angle(M, ang, height, mag, width):
    """
    Calculate median angle and magnitude of optical flow vectors inside the cropped area

    :param M: Output of cv2.getPerspectiveTransform()
    :param ang: array of angles of the optical flow vectors
    :param height: height of the cropped area
    :param mag: array of magnitudes of the optical flow vectors
    :param width: width of the cropped area
    :return: median angle, median magnitude and mean magnitude of optical flow vectors
    """
    warped_ang = cv2.warpPerspective(ang, M, (width, height))
    warped_mag = cv2.warpPerspective(mag, M, (width, height))
    median_angle = math.radians(np.median(warped_ang[warped_mag != 0]))
    mean_magnitude = np.mean(warped_mag)
    median_magnitude = np.median(warped_mag)
    return median_angle, median_magnitude, mean_magnitude


def full_pipeline_demo(method, video_path, model_path, class_label,
                       visualize_results, no_bb_to_save, skip_inference,
                       params=[], to_gray=False, tvl1=False, use_heatmap_masking=False, resolution=(720, 1280)):
    """
    Processes the input video's frames one by one and calculates optical flow between the consecutive frames.
    For each frame in the input video, the algorithm tries to extract a bounding box based on the optical flow heatmap
    calculated between two consecutive frames.
    After the video has been fully processed, the extracted bounding boxes are sorted based on their area and
    the median elements are chosen as candidates for object recognition.
    The object recognition inference is performed with two different approaches: One processes each cropped image
    separately and then aggregates the separate results together. The other takes all images as input together and
    calculates all of their latent vectors' distances from the class prototype vectors. The minimum distance between
    all the input images and the object classes is chosen as the result of object recognition.

    :param method: Selected optical flow method, e.g.: cv2.optflow.calcOpticalFlowSparseToDense
    :param video_path: Path to the input video file
    :param model_path: Path to the few shot learner model file
    :param class_label: Label of the object of interest in the input video
    :param visualize_results: If True, frames are displayed on screen with bounding box overlaid
    :param no_bb_to_save: Number of bounding boxes to save at the end of object detection
    :param skip_inference: If True, FSL inference is skipped
    :param params: Optional; additional parameters of the selected method
    :param to_gray: Optional; if True: converts the input video's frames to grayscale
                    (some optical flow methods require it)
    :param tvl1: Set to True if the selected method is tvl1, i.e.: cv2.optflow.DualTVL1OpticalFlow_create
    :param use_heatmap_masking: Optional; If True: optical flow heatmap is used as a mask to crop the object
    :param resolution: Resolution of the frames for resizing
    """

    print(f"Running inference after object detection: {not skip_inference}")
    print(f"Visualizing results on screen: {visualize_results}")

    target_folder_path = f"../cropped_bbs/"

    if os.path.isdir(video_path):
        paths = [os.path.join(video_path, f) for f in os.listdir(video_path)]
    else:
        paths = [video_path]

    for path in tqdm(paths):
        cropped_bb_list = []
        filename = "tmp"
        img_head, img_tail = os.path.split(path)

        if class_label is None:
            try:  # try to get class label from video path if exists
                class_label, filename = img_tail[:-4].split("_")
            except ValueError:
                pass

        print(f"Processing video: {img_tail}")

        no_folders_in_target_folder = len([x[0] for x in os.walk(target_folder_path)])
        target_output_folder = f"{target_folder_path}{filename}_{no_folders_in_target_folder}"
        os.makedirs(target_output_folder, exist_ok=True)
        os.makedirs(f"{target_output_folder}/csv_files/", exist_ok=True)
        print(f"Output folder: {target_output_folder}")

        # read the video
        cap = cv2.VideoCapture(path)
        orientation = cap.get(cv2.CAP_PROP_ORIENTATION_META)
        # Read the first frame
        ret, old_frame = cap.read()

        old_frame = cv2.resize(old_frame, resolution)
        if orientation == 270.0:
            old_frame = cv2.rotate(old_frame, cv2.ROTATE_180)

        hsv = np.zeros_like(old_frame)
        hsv[..., 1] = 255

        # Preprocessing for exact method
        if to_gray:
            old_frame = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)

        frame_counter = 0
        while True:
            # Read the next frame
            ret, new_frame = cap.read()
            if new_frame is not None:
                new_frame = cv2.resize(new_frame, resolution)
            if orientation == 270.0:
                new_frame = cv2.rotate(new_frame, cv2.ROTATE_180)
            if not ret:
                break

            frame_copy = new_frame.copy()
            frame_copy2 = new_frame.copy()

            # Preprocessing for exact method
            if to_gray:
                new_frame = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
            # Calculate Optical Flow
            if tvl1:
                flow = method.calc(old_frame, new_frame, None, *params)
            else:
                flow = method(old_frame, new_frame, None, *params)

            # cartToPolar: Calculates the magnitude and angle of 2D vectors
            mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1], angleInDegrees=True)

            # Use Hue and Value to encode the Optical Flow
            hsv[..., 0] = ang  # values: [0, 360]
            hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)  # values: [0, 255]

            # Convert HSV image into BGR for demo
            bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

            # Visualize the motion vectors on top of the input frame
            vectors = draw_flow(frame_copy.copy(), flow, to_gray=to_gray, step=16)

            # Blur the optical flow heatmap to reduce the number of separate motion regions
            bgr = cv2.medianBlur(bgr, 55)

            # Overlay the motion vectors on the optical flow heatmap for visualization purposes
            combined_flow_img = cv2.addWeighted(vectors, 0.2, bgr, 0.8, 0)

            k = cv2.waitKey(25) & 0xFF
            if k == 27:  # If ESC is pressed, break from the optical flow calculation
                break

            _, thresholded_img = cv2.threshold(cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY), 10, 255, cv2.THRESH_BINARY)
            # Use a contour-finding algorithm to find connected-regions on the heatmap
            contours, hierarchy = cv2.findContours(thresholded_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            # Find the bounding box around the largest contour region
            bgr, frame_with_bb, ret_bb, axes_aligned_bb_coordinates = identify_and_draw_bounding_box(contours, frame_copy,
                                                                                                     frame_copy, draw=False)
            axes_aligned_bb_coordinates.insert(0, f"{frame_counter}.png")
            if len(ret_bb) != 0:  # Rotate the image according to the rotated bounding box and crop
                (x, y), (w, h), a = ret_bb[0]
                width = int(w)
                height = int(h)
                box = cv2.boxPoints(ret_bb[0])
                box = np.int0(box)
                src_pts = box.astype("float32")
                dst_pts = np.array([[0, height - 1],
                                    [0, 0],
                                    [width - 1, 0],
                                    [width - 1, height - 1]], dtype="float32")
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                warped = cv2.warpPerspective(frame_copy2, M, (width, height))

                if use_heatmap_masking:  # Masks the cropped image with the intensities of the heatmap
                    warped_heatmap = cv2.warpPerspective(bgr, M, (width, height))
                    warped_heatmap = cv2.cvtColor(warped_heatmap, cv2.COLOR_BGR2GRAY)
                    warped_heatmap = cv2.normalize(warped_heatmap.astype('float32'), None, 0.0, 1.0,
                                                   cv2.NORM_MINMAX)
                    warped_heatmap = np.sqrt(warped_heatmap)
                    warped = (warped.astype('float32') * np.expand_dims(warped_heatmap, axis=2)).astype('uint8')

                # Calculate the motion vector median angle and magnitude in the cropped image
                med_ang, median_magnitude, mean_magnitude = calc_cropped_relative_median_angle(M, ang, height, mag, width)

                med_ang = math.degrees(med_ang)
                # print("Median angle of cropped: ", med_ang)
                # print("Mean magnitude of cropped: ", mean_magnitude)
                # print("Median magnitude of cropped: ", median_magnitude)

                keep_bb = False
                # Filter the cropped image based on the median motion vector angles and magnitudes
                angle_filter = 160 <= med_ang <= 200 or 340 <= med_ang <= 360 or 0 <= med_ang <= 20
                mag_filter = median_magnitude >= 15
                if mag_filter or (angle_filter and median_magnitude >= 5):
                    keep_bb = True

                if keep_bb:  # Draw rotated bounding box on screen
                    combined_flow_img = cv2.drawContours(combined_flow_img, [box], 0, (0, 0, 255), 3)
                cropped_bb_list.append(  # Add cropped image to list, also its frame counter and area
                    [cv2.rotate(warped, cv2.ROTATE_90_CLOCKWISE), frame_counter,
                     warped.shape[0] * warped.shape[1], axes_aligned_bb_coordinates, frame_with_bb])

            if visualize_results:
                cv2.imshow("optical flow", combined_flow_img)

            old_frame = new_frame
            frame_counter += 1

        cropped_bb_list.sort(key=lambda elem_: elem_[2])
        median_element = len(cropped_bb_list) // 2
        min_id = max(median_element - (no_bb_to_save // 2), 0)
        max_id = min(median_element + (no_bb_to_save // 2), len(cropped_bb_list))

        if not skip_inference:
            print("Inference with post-processing ...")
            final_prediction_top1 = []
            final_prediction_top5 = []
            for bb_i in range(min_id, max_id):
                # Save cropped images to folder
                cv2.imwrite(f"{target_output_folder}/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][0])
                cv2.imwrite(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][4])
                csv_file = open(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.csv", 'w')
                writer = csv.writer(csv_file)
                writer.writerow(cropped_bb_list[bb_i][3])
                csv_file.close()

                top1_prediction_class_name, all_distances = \
                    few_shot_learner_api.inference(model_path=model_path,
                                                   input_image=f"{target_output_folder}/{cropped_bb_list[bb_i][1]}.png",  # cv2.cvtColor(cropped_bb_list[bb_i][0], cv2.COLOR_BGR2RGB)
                                                   annotation_name=f"{class_label}",
                                                   force_cpu=True,
                                                   show_plot=False,
                                                   verbose=False)
                final_prediction_top1.append(top1_prediction_class_name)
                for elem in all_distances[:5]:
                    final_prediction_top5.append(elem[0])

            final_prediction_top1_counter = Counter(final_prediction_top1)
            final_prediction_top5_counter = Counter(final_prediction_top5)
            top1_verdict = ""
            top5_verdict = ""
            if class_label is not None:
                top1_verdict = "CORRECT" if class_label == final_prediction_top1_counter.most_common(1)[0][0] else "INCORRECT"
                top5_verdict = "CORRECT" if class_label in [label for label, count in
                                                            final_prediction_top5_counter.most_common(5)] else "INCORRECT"
            print(f"Final prediction from {len(final_prediction_top1)} images:")
            print(f"based on TOP1 predictions: {top1_verdict} - {final_prediction_top1_counter.most_common(1)[0][0]}")
            print(f"based on TOP5 predictions: {top5_verdict} - {final_prediction_top5_counter.most_common(5)}")
            print(final_prediction_top5_counter)

            print("Inference with multi-frame cluster-distance calculation ...")
            top1_prediction_class_name, all_distances =\
                few_shot_learner_api.multi_frame_inference(model_path=model_path,
                                                           input_image_list=f"{target_output_folder}/",  # cropped_bb_list[min_id:max_id],
                                                           annotation_name=f"{class_label}",
                                                           force_cpu=True,
                                                           show_plot=False,
                                                           verbose=False)

            if class_label is not None:
                top1_verdict = "CORRECT" if class_label == top1_prediction_class_name else "INCORRECT"
                top5_verdict = "CORRECT" if class_label in [label_dist_pair[0] for label_dist_pair in all_distances[:5]] else "INCORRECT"
            print(f"Final prediction from {len(cropped_bb_list[min_id:max_id])} images:")
            print(f"based on TOP1 predictions: {top1_verdict} - {top1_prediction_class_name}")
            print(f"based on TOP5 predictions: {top5_verdict} - {all_distances[:5]}")
            print(all_distances)

        else:  # Only save cropped images to folder, no inference
            for bb_i in range(min_id, max_id):
                cv2.imwrite(f"{target_output_folder}/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][0])
                cv2.imwrite(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][4])
                csv_file = open(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.csv", 'w')
                writer = csv.writer(csv_file)
                writer.writerow(cropped_bb_list[bb_i][3])
                csv_file.close()


def combined_yolo_optical_flow(method, video_path, model_path, yolo_model_path, class_label,
                               visualize_results, no_bb_to_save, skip_inference, yolo_save_every_frame, n_of_sharp_imgs,
                               params=[], to_gray=False, tvl1=False, use_heatmap_masking=False, resolution=(720, 1280)):
    """
    Processes the input video's frames one by one and calculates optical flow between the consecutive frames.
    For each frame in the input video, the algorithm tries to extract a bounding box based on the optical flow heatmap
    calculated between two consecutive frames.
    After the video has been fully processed, the extracted bounding boxes are sorted based on their area and
    the median elements are chosen as candidates for object recognition.
    The object recognition inference is performed with two different approaches: One processes each cropped image
    separately and then aggregates the separate results together. The other takes all images as input together and
    calculates all of their latent vectors' distances from the class prototype vectors. The minimum distance between
    all the input images and the object classes is chosen as the result of object recognition.

    :param method: Selected optical flow method, e.g.: cv2.optflow.calcOpticalFlowSparseToDense
    :param video_path: Path to the input video file
    :param model_path: Path to the few shot learner model file
    :param yolo_model_path: Path to the yolo-v7 object detection model file
    :param class_label: Label of the object of interest in the input video
    :param visualize_results: If True, frames are displayed on screen with bounding box overlaid
    :param no_bb_to_save: Number of bounding boxes to save at the end of object detection
    :param skip_inference: If True, FSL inference is skipped
    :param yolo_save_every_frame: If specified, the result for every frame will be saved
                                  during yolo-optical flow combined object detection
    :param n_of_sharp_imgs: number of least blurry images to pick
    :param params: Optional; additional parameters of the selected method
    :param to_gray: Optional; if True: converts the input video's frames to grayscale
                    (some optical flow methods require it)
    :param tvl1: Set to True if the selected method is tvl1, i.e.: cv2.optflow.DualTVL1OpticalFlow_create
    :param use_heatmap_masking: Optional; If True: optical flow heatmap is used as a mask to crop the object
    :param resolution: Resolution of the frames for resizing
    """

    print(f"Running inference after object detection: {not skip_inference}")
    print(f"Visualizing results on screen: {visualize_results}")
    print(f"Saving combined object detection results for every frame : {yolo_save_every_frame}")

    # < ------------ LOAD YOLO MODEL ---------- >
    yolo_model = yolo_detect.load_yolo_model(weights=yolo_model_path,
                                             device="cpu",
                                             verbose=False)
    # < ------------ LOAD YOLO MODEL ---------- >
    target_folder_path = f"./cropped_bbs/"

    if os.path.isdir(video_path):
        paths = [os.path.join(video_path, f) for f in os.listdir(video_path)]
    else:
        paths = [video_path]

    top1_accuracy_counter = 0
    top5_accuracy_counter = 0
    top25_accuracy_counter = 0
    top1_accuracy_counter_multi_cluster = 0
    top5_accuracy_counter_multi_cluster = 0
    top25_accuracy_counter_multi_cluster = 0
    for path in tqdm(paths):
        cropped_bb_list = []
        iou_list = []

        img_head, img_tail = os.path.split(path)

        if class_label is None:
            try:  # try to get class label from video path if exists
                class_label, filename = img_tail[:-4].split("_")
            except ValueError:
                filename = f"{os.path.basename(os.path.normpath(img_head))}_{img_tail}"
        else:
            filename = f"{os.path.basename(os.path.normpath(img_head))}_{img_tail}"

        print(f"Processing video: {img_tail}")
        no_folders_in_target_folder = len(next(os.walk(target_folder_path))[1])
        target_output_folder = f"{target_folder_path}{filename}_{no_folders_in_target_folder}"
        os.makedirs(target_output_folder, exist_ok=True)
        os.makedirs(f"{target_output_folder}/csv_files/", exist_ok=True)
        os.makedirs(f"{target_output_folder}/combined_detections/", exist_ok=True)
        os.makedirs(f"{target_output_folder}/combined_detections/every_frame", exist_ok=True)
        os.makedirs(f"{target_output_folder}/combined_detections/cropped_images", exist_ok=True)
        os.makedirs(f"{target_output_folder}/combined_detections/optical_flow_crops", exist_ok=True)
        os.makedirs(f"{target_output_folder}/combined_detections/high_iou_yolo_crops", exist_ok=True)
        os.makedirs(f"{target_output_folder}/combined_detections/averaged_bb_crops", exist_ok=True)
        print(f"Output folder: {target_output_folder}")

        # read the video
        cap = cv2.VideoCapture(path)
        orientation = cap.get(cv2.CAP_PROP_ORIENTATION_META)
        # Read the first frame
        ret, old_frame = cap.read()

        old_frame = cv2.resize(old_frame, resolution)
        if orientation == 270.0:
            old_frame = cv2.rotate(old_frame, cv2.ROTATE_180)

        hsv = np.zeros_like(old_frame)
        hsv[..., 1] = 255

        # Preprocessing for exact method
        if to_gray:
            old_frame = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)

        frame_counter = 0
        while True:
            # Read the next frame
            ret, new_frame = cap.read()
            if new_frame is not None:
                new_frame = cv2.resize(new_frame, resolution)
            if orientation == 270.0:
                new_frame = cv2.rotate(new_frame, cv2.ROTATE_180)
            if not ret:
                break

            frame_copy = new_frame.copy()
            clean_frame = new_frame.copy()

            # Preprocessing for exact method
            if to_gray:
                new_frame = cv2.cvtColor(new_frame, cv2.COLOR_BGR2GRAY)
            # Calculate Optical Flow
            if tvl1:
                flow = method.calc(old_frame, new_frame, None, *params)
            else:
                flow = method(old_frame, new_frame, None, *params)

            # cartToPolar: Calculates the magnitude and angle of 2D vectors
            mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1], angleInDegrees=True)

            # Use Hue and Value to encode the Optical Flow
            hsv[..., 0] = ang  # values: [0, 360]
            hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)  # values: [0, 255]

            # Convert HSV image into BGR for demo
            bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

            # Visualize the motion vectors on top of the input frame
            vectors = draw_flow(frame_copy.copy(), flow, to_gray=to_gray, step=16)

            # Blur the optical flow heatmap to reduce the number of separate motion regions
            bgr = cv2.medianBlur(bgr, 55)

            # Overlay the motion vectors on the optical flow heatmap for visualization purposes
            combined_flow_img = cv2.addWeighted(vectors, 0.2, bgr, 0.8, 0)

            k = cv2.waitKey(25) & 0xFF
            if k == 27:  # If ESC is pressed, break from the optical flow calculation
                break

            _, thresholded_img = cv2.threshold(cv2.cvtColor(bgr, cv2.COLOR_BGR2GRAY), 10, 255, cv2.THRESH_BINARY)
            # Use a contour-finding algorithm to find connected-regions on the heatmap
            contours, hierarchy = cv2.findContours(thresholded_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            # Find the bounding box around the largest contour region
            bgr, frame_with_bb, ret_bb, axes_aligned_bb = identify_and_draw_bounding_box(contours, frame_copy,
                                                                                         frame_copy, draw=False)
            axes_aligned_bb.insert(0, f"{frame_counter}.png")
            if len(ret_bb) != 0:  # Rotate the image according to the rotated bounding box and crop
                (x, y), (w, h), a = ret_bb[0]
                width = int(w)
                height = int(h)
                box = cv2.boxPoints(ret_bb[0])
                box = np.int0(box)
                src_pts = box.astype("float32")
                dst_pts = np.array([[0, height - 1],
                                    [0, 0],
                                    [width - 1, 0],
                                    [width - 1, height - 1]], dtype="float32")
                M = cv2.getPerspectiveTransform(src_pts, dst_pts)
                warped = cv2.warpPerspective(clean_frame, M, (width, height))

                if use_heatmap_masking:  # Masks the cropped image with the intensities of the heatmap
                    warped_heatmap = cv2.warpPerspective(bgr, M, (width, height))
                    warped_heatmap = cv2.cvtColor(warped_heatmap, cv2.COLOR_BGR2GRAY)
                    warped_heatmap = cv2.normalize(warped_heatmap.astype('float32'), None, 0.0, 1.0,
                                                   cv2.NORM_MINMAX)
                    warped_heatmap = np.sqrt(warped_heatmap)
                    warped = (warped.astype('float32') * np.expand_dims(warped_heatmap, axis=2)).astype('uint8')

                # Calculate the motion vector median angle and magnitude in the cropped image
                # med_ang, median_magnitude, mean_magnitude = calc_cropped_relative_median_angle(M, ang, height, mag, width)

                # med_ang = math.degrees(med_ang)
                # print("Median angle of cropped: ", med_ang)
                # print("Mean magnitude of cropped: ", mean_magnitude)
                # print("Median magnitude of cropped: ", median_magnitude)

                # keep_bb = False
                # # Filter the cropped image based on the median motion vector angles and magnitudes
                # angle_filter = 160 <= med_ang <= 200 or 340 <= med_ang <= 360 or 0 <= med_ang <= 20
                # mag_filter = median_magnitude >= 15
                # if mag_filter or (angle_filter and median_magnitude >= 5):
                #     keep_bb = True

                # < ------------ YOLO ---------- >

                # Draw Optical-flow based object detection on image
                combined_flow_img = cv2.rectangle(combined_flow_img, (int(axes_aligned_bb[1]), int(axes_aligned_bb[2])),
                                                  (int(axes_aligned_bb[3]), int(axes_aligned_bb[4])), (0, 0, 255), 2)
                cv2.putText(combined_flow_img, f"Optical Flow", (int(axes_aligned_bb[1]), int(axes_aligned_bb[2]) - 2),
                            0, 1, [0, 0, 255], thickness=2, lineType=cv2.LINE_AA)
                cv2.putText(frame_with_bb, f"Optical Flow", (int(axes_aligned_bb[1]), int(axes_aligned_bb[2]) - 2),
                            0, 1, [0, 0, 255], thickness=2, lineType=cv2.LINE_AA)

                confidences, detected_bounding_boxes = yolo_detect.detect(model=yolo_model,
                                                                          conf_thres=0.25,
                                                                          source=clean_frame,
                                                                          device="cpu")
                if len(confidences) != 0:
                    yolo_bb = detected_bounding_boxes[confidences.index(max(confidences))]
                    yolo_bb = [tensor.item() for tensor in yolo_bb]

                    iou = bb_intersection_over_union(axes_aligned_bb[1:], yolo_bb)
                    combined_bb_box = np.mean(np.array([axes_aligned_bb[1:], yolo_bb]), axis=0)
                    if iou > 0.0:
                        combined_flow_img = cv2.rectangle(combined_flow_img, (int(combined_bb_box[0]), int(combined_bb_box[1])),
                                                          (int(combined_bb_box[2]), int(combined_bb_box[3])), (255, 0, 255), 2)
                        frame_with_bb = cv2.rectangle(frame_with_bb, (int(combined_bb_box[0]), int(combined_bb_box[1])),
                                                      (int(combined_bb_box[2]), int(combined_bb_box[3])), (255, 0, 255), 2)
                        cv2.putText(combined_flow_img, f"AVERAGED", (int(combined_bb_box[0]), int(combined_bb_box[1]) - 2),
                                    0, 1, [255, 0, 255], thickness=2, lineType=cv2.LINE_AA)
                        cv2.putText(frame_with_bb, f"AVERAGED", (int(combined_bb_box[0]), int(combined_bb_box[1]) - 2),
                                    0, 1, [255, 0, 255], thickness=2, lineType=cv2.LINE_AA)

                    # Draw YOLO bounding box on image
                    combined_flow_img = cv2.rectangle(combined_flow_img, (int(yolo_bb[0]), int(yolo_bb[1])),
                                                      (int(yolo_bb[2]), int(yolo_bb[3])), (255, 0, 0), 2)
                    frame_with_bb = cv2.rectangle(frame_with_bb, (int(yolo_bb[0]), int(yolo_bb[1])),
                                                  (int(yolo_bb[2]), int(yolo_bb[3])), (255, 0, 0), 2)
                    cv2.putText(combined_flow_img, f"YOLO", (int(yolo_bb[0]), int(yolo_bb[1]) - 2),
                                0, 1, [255, 0, 0], thickness=2, lineType=cv2.LINE_AA)
                    cv2.putText(frame_with_bb, f"YOLO", (int(yolo_bb[0]), int(yolo_bb[1]) - 2),
                                0, 1, [255, 0, 0], thickness=2, lineType=cv2.LINE_AA)

                    # yolo_bb = scale_coords(resolution, yolo_bb, higher_resolution).round()
                    # combined_bb_box = scale_coords(resolution, combined_bb_box, higher_resolution).round()

                    iou_list.append([iou, frame_counter, combined_flow_img, frame_with_bb,
                                     clean_frame[int(axes_aligned_bb[2]):int(axes_aligned_bb[4]), int(axes_aligned_bb[1]):int(axes_aligned_bb[3])],
                                     clean_frame[int(yolo_bb[1]):int(yolo_bb[3]), int(yolo_bb[0]):int(yolo_bb[2])],
                                     clean_frame[int(combined_bb_box[1]):int(combined_bb_box[3]), int(combined_bb_box[0]):int(combined_bb_box[2])]])
                    if iou > 0.5:
                        cv2.putText(combined_flow_img, f"IoU: {iou:.2f}",
                                    (0, 40), 0, 1, [0, 255, 0], thickness=2, lineType=cv2.LINE_AA)
                        cv2.putText(frame_with_bb, f"IoU: {iou:.2f}",
                                    (0, 40), 0, 1, [0, 255, 0], thickness=2, lineType=cv2.LINE_AA)
                    else:
                        cv2.putText(combined_flow_img, f"IoU: {iou:.2f}",
                                    (0, 40), 0, 1, [0, 0, 255], thickness=2, lineType=cv2.LINE_AA)
                        cv2.putText(frame_with_bb, f"IoU: {iou:.2f}",
                                    (0, 40), 0, 1, [0, 0, 255], thickness=2, lineType=cv2.LINE_AA)

                else:
                    yolo_bb = None
                # < ------------ YOLO ---------- >

                # if keep_bb:  # Draw rotated bounding box on screen
                #     combined_flow_img = cv2.drawContours(combined_flow_img, [box], 0, (0, 0, 255), 3)
                cropped_bb_list.append(  # Add cropped image to list, also its frame counter and area
                    [clean_frame[int(axes_aligned_bb[2]):int(axes_aligned_bb[4]), int(axes_aligned_bb[1]):int(axes_aligned_bb[3])],
                     frame_counter, warped.shape[0] * warped.shape[1], axes_aligned_bb, frame_with_bb, yolo_bb])

                if yolo_save_every_frame:
                    if len(axes_aligned_bb) > 1:
                        cv2.imwrite(f"{target_output_folder}/combined_detections/every_frame/{frame_counter}.png",
                                    frame_with_bb)
                        csv_file1 = open(f"{target_output_folder}/combined_detections/every_frame/{frame_counter}_optical_flow.csv", 'w')
                        writer = csv.writer(csv_file1)
                        writer.writerow(axes_aligned_bb)
                        csv_file1.close()
                    if yolo_bb is not None:
                        csv_file2 = open(f"{target_output_folder}/combined_detections/every_frame/{frame_counter}_yolo.csv", 'w')
                        writer = csv.writer(csv_file2)
                        yolo_bb.insert(0, f"{frame_counter}.png")
                        writer.writerow(yolo_bb)
                        csv_file2.close()

            if visualize_results:
                cv2.imshow("optical flow", combined_flow_img)

            old_frame = new_frame
            frame_counter += 1

        iou_list.sort(key=lambda elem_: elem_[0], reverse=True)
        for i in range(0, min(no_bb_to_save, len(iou_list))):
            cv2.imwrite(f"{target_output_folder}/combined_detections/{iou_list[i][1]}.png",
                        iou_list[i][2])
            cv2.imwrite(f"{target_output_folder}/combined_detections/{iou_list[i][1]}_original.png",
                        iou_list[i][3])
            cv2.imwrite(f"{target_output_folder}/combined_detections/cropped_images/{iou_list[i][1]}_cropped_optical_flow.png",
                        iou_list[i][4])
            cv2.imwrite(f"{target_output_folder}/combined_detections/cropped_images/{iou_list[i][1]}_cropped_yolo.png",
                        iou_list[i][5])
            cv2.imwrite(
                f"{target_output_folder}/combined_detections/optical_flow_crops/{iou_list[i][1]}_cropped_optical_flow.png",
                iou_list[i][4])
            cv2.imwrite(f"{target_output_folder}/combined_detections/high_iou_yolo_crops/{iou_list[i][1]}_cropped_yolo.png",
                        iou_list[i][5])
            cv2.imwrite(f"{target_output_folder}/combined_detections/averaged_bb_crops/{iou_list[i][1]}_cropped_averaged.png",
                        iou_list[i][6])

        least_blurry_yolo_img_paths = blur_detector.find_n_least_blurry_imgs(
            f"{target_output_folder}/combined_detections/high_iou_yolo_crops/", N=n_of_sharp_imgs, visualize=False)
        os.makedirs(f"{target_output_folder}/combined_detections/high_iou_yolo_crops/n_sharpest_crops", exist_ok=True)
        for img_path in least_blurry_yolo_img_paths:
            shutil.copyfile(os.path.join(f"{target_output_folder}/combined_detections/high_iou_yolo_crops/", img_path),
                            os.path.join(f"{target_output_folder}/combined_detections/high_iou_yolo_crops/n_sharpest_crops", img_path))

        cropped_bb_list.sort(key=lambda elem_: elem_[2])
        median_element = len(cropped_bb_list) // 2
        min_id = max(median_element - (no_bb_to_save // 2), 0)
        max_id = min(median_element + (no_bb_to_save // 2), len(cropped_bb_list))

        if not skip_inference:
            print("Inference with post-processing ...")
            final_prediction_top1 = []
            final_prediction_top5 = []
            final_prediction_top25 = []
            avg_iou = []
            for bb_i in range(min_id, max_id):

                # < ------------ YOLO TEST ---------- >
                iou = bb_intersection_over_union(cropped_bb_list[bb_i][3][1:], cropped_bb_list[bb_i][5])
                avg_iou.append(iou)

                if cropped_bb_list[bb_i][5] is not None:
                    # Draw YOLO bounding box on image
                    cropped_bb_list[bb_i][4] = cv2.rectangle(cropped_bb_list[bb_i][4],
                                                             (int(cropped_bb_list[bb_i][5][0]), int(cropped_bb_list[bb_i][5][1])),
                                                             (int(cropped_bb_list[bb_i][5][2]), int(cropped_bb_list[bb_i][5][3])),
                                                             (255, 0, 0), 2)

                    cv2.putText(cropped_bb_list[bb_i][4], f"YOLO",
                                (int(cropped_bb_list[bb_i][5][0]), int(cropped_bb_list[bb_i][5][1]) - 2),
                                0, 1, [255, 0, 0], thickness=2, lineType=cv2.LINE_AA)

                    cv2.putText(cropped_bb_list[bb_i][4], f"IoU: {iou:.2f}",
                                (0, 40), 0, 1, [0, 0, 255], thickness=2, lineType=cv2.LINE_AA)
                # < ------------ YOLO TEST ---------- >

                # Save cropped images to folder
                cv2.imwrite(f"{target_output_folder}/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][0])
                cv2.imwrite(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][4])
                csv_file = open(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.csv", 'w')
                writer = csv.writer(csv_file)
                writer.writerow(cropped_bb_list[bb_i][3])
                csv_file.close()

                top1_prediction_class_name, all_distances = \
                    few_shot_learner_api.inference(model_path=model_path,
                                                   input_image=f"{target_output_folder}/{cropped_bb_list[bb_i][1]}.png",  # cv2.cvtColor(cropped_bb_list[bb_i][0], cv2.COLOR_BGR2RGB)
                                                   annotation_name=f"{class_label}",
                                                   force_cpu=True,
                                                   show_plot=False,
                                                   verbose=False)
                final_prediction_top1.append(top1_prediction_class_name)
                for elem in all_distances[:5]:
                    final_prediction_top5.append(elem[0])
                for elem in all_distances[:25]:
                    final_prediction_top25.append(elem[0])

            print(f"Average IoU: {np.mean(avg_iou)}")

            final_prediction_top1_counter = Counter(final_prediction_top1)
            final_prediction_top5_counter = Counter(final_prediction_top5)
            final_prediction_top25_counter = Counter(final_prediction_top25)
            top1_verdict = ""
            top5_verdict = ""
            top25_verdict = ""
            if class_label is not None:
                top1_verdict = "CORRECT" if class_label == final_prediction_top1_counter.most_common(1)[0][0] else "INCORRECT"
                top5_verdict = "CORRECT" if class_label in [label for label, count in
                                                            final_prediction_top5_counter.most_common(5)] else "INCORRECT"
                top25_verdict = "CORRECT" if class_label in [label for label, count in
                                                            final_prediction_top25_counter.most_common(25)] else "INCORRECT"
            top1_accuracy_counter += int(top1_verdict == "CORRECT")
            top5_accuracy_counter += int(top5_verdict == "CORRECT")
            top25_accuracy_counter += int(top25_verdict == "CORRECT")

            print(f"Final prediction from {len(final_prediction_top1)} images:")
            print(f"based on TOP1 predictions: {top1_verdict} - {final_prediction_top1_counter.most_common(1)[0][0]}")
            print(f"based on TOP5 predictions: {top5_verdict} - {final_prediction_top5_counter.most_common(5)}")
            print(f"based on TOP25 predictions: {top25_verdict} - {final_prediction_top5_counter.most_common(25)}")
            # print(final_prediction_top5_counter)

            print("Inference with multi-frame cluster-distance calculation ...")
            top1_prediction_class_name, all_distances =\
                few_shot_learner_api.multi_frame_inference(model_path=model_path,
                                                           input_image_list=f"{target_output_folder}/combined_detections/cropped_images/",  # cropped_bb_list[min_id:max_id],
                                                           annotation_name=f"{class_label}",
                                                           force_cpu=True,
                                                           show_plot=False,
                                                           verbose=False)

            top1_verdict = ""
            top5_verdict = ""
            top25_verdict = ""
            if class_label is not None:
                top1_verdict = "CORRECT" if class_label == top1_prediction_class_name else "INCORRECT"
                top5_verdict = "CORRECT" if class_label in [label_dist_pair[0] for label_dist_pair in all_distances[:5]] else "INCORRECT"
                top25_verdict = "CORRECT" if class_label in [label_dist_pair[0] for label_dist_pair in all_distances[:25]] else "INCORRECT"

            top1_accuracy_counter_multi_cluster += int(top1_verdict == "CORRECT")
            top5_accuracy_counter_multi_cluster += int(top5_verdict == "CORRECT")
            top25_accuracy_counter_multi_cluster += int(top25_verdict == "CORRECT")

            print(f"Final prediction from {len(cropped_bb_list[min_id:max_id])} images:")
            print(f"based on TOP1 predictions: {top1_verdict} - {top1_prediction_class_name}")
            print(f"based on TOP5 predictions: {top5_verdict} - {all_distances[:5]}")
            print(f"based on TOP25 predictions: {top25_verdict} - {all_distances[:25]}")
            # print(all_distances)

        else:  # Only save cropped images to folder, no inference
            for bb_i in range(min_id, max_id):
                cv2.imwrite(f"{target_output_folder}/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][0])
                cv2.imwrite(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.png", cropped_bb_list[bb_i][4])
                csv_file = open(f"{target_output_folder}/csv_files/{cropped_bb_list[bb_i][1]}.csv", 'w')
                writer = csv.writer(csv_file)
                writer.writerow(cropped_bb_list[bb_i][3])
                csv_file.close()

    print(f"TOP1 accuracy: {float(top1_accuracy_counter) / float(len(paths))}")
    print(f"TOP5 accuracy: {float(top5_accuracy_counter) / float(len(paths))}")
    print(f"TOP25 accuracy: {float(top25_accuracy_counter) / float(len(paths))}")
    print(f"TOP1 multi cluster accuracy: {float(top1_accuracy_counter_multi_cluster) / float(len(paths))}")
    print(f"TOP5 multi cluster accuracy: {float(top5_accuracy_counter_multi_cluster) / float(len(paths))}")
    print(f"TOP25 multi cluster accuracy: {float(top25_accuracy_counter_multi_cluster) / float(len(paths))}")