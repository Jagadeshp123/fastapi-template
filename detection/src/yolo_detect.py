import argparse
import time
from pathlib import Path
import warnings

import cv2
import torch
import numpy as np
import torch.backends.cudnn as cudnn
from numpy import random

from detection.src.models.experimental import attempt_load
from detection.src.utils.datasets import LoadStreams, LoadImages, letterbox
from detection.src.utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, increment_path
from detection.src.utils.plots import plot_one_box
from detection.src.utils.torch_utils import select_device, load_classifier, time_synchronized, TracedModel


warnings.filterwarnings("ignore")


def load_yolo_model(weights='yolov7.pt', trace=True, device='', img_size=640, verbose=False):
    # Load model
    print(weights,"line25")
    # weights = 'detection/best.pt'
    model = attempt_load(weights, map_location=device)  # load FP32 model
    device = select_device(device)
    half = device.type != 'cpu'  # half precision only supported on CUDA
    if trace:
        model = TracedModel(model, device, img_size, verbose)

    if half:
        model.half()  # to FP16

    return model


def detect(source='inference/images', imgsz=640, device='', augment=False,
           conf_thres=0.25, iou_thres=0.45, classes=None, agnostic_nms=False, model=None):

    confidence_results = []
    detected_bounding_boxes = []

    # Initialize
    set_logging()
    device = select_device(device)
    half = device.type != 'cpu'  # half precision only supported on CUDA
    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(imgsz, s=stride)  # check img_size

    if not isinstance(source, str):
        # Padded resize
        source_resized = letterbox(source, imgsz, stride=stride)[0]

        # Convert
        source_resized = source_resized[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(source_resized)
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)
        # Inference
        pred = model(img, augment=augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, conf_thres, iou_thres, classes=classes, agnostic=agnostic_nms)

        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], source.shape).round()

                # Write results
                for *xyxy, conf, cls in reversed(det):
                    confidence_results.append(round(float(conf), 3))
                    detected_bounding_boxes.append(xyxy)

    else:
        # Set Dataloader
        dataset = LoadImages(source, img_size=imgsz, stride=stride)

        for path, img, im0s, _ in dataset:
            img = torch.from_numpy(img).to(device)
            img = img.half() if half else img.float()  # uint8 to fp16/32
            img /= 255.0  # 0 - 255 to 0.0 - 1.0
            if img.ndimension() == 3:
                img = img.unsqueeze(0)

            # Inference
            with torch.no_grad():   # Calculating gradients would cause a GPU memory leak
                pred = model(img, augment=augment)[0]

            # Apply NMS
            pred = non_max_suppression(pred, conf_thres, iou_thres, classes=classes, agnostic=agnostic_nms)

            # Process detections
            for i, det in enumerate(pred):  # detections per image
                if len(det):
                    # Rescale boxes from img_size to im0 size
                    det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0s.shape).round()

                    # Write results
                    for *xyxy, conf, cls in reversed(det):
                        confidence_results.append(round(float(conf), 3))
                        detected_bounding_boxes.append(xyxy)

    return confidence_results, detected_bounding_boxes


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='yolov7.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='inference/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.25, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--no-trace', action='store_true', help='don`t trace model')
    parser.add_argument('--verbose', action='store_true', help='print messages')
    opt = parser.parse_args()
    if opt.verbose:
        print(opt)
    # check_requirements(exclude=('pycocotools', 'thop'))

    with torch.no_grad():

        model = load_yolo_model(weights='yolov7.pt', trace=not opt.no_trace, device=opt.device,
                                img_size=opt.img_size, verbose=opt.verbose)

        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov7.pt']:
                detect(opt.source, opt.img_size, opt.device, opt.augment, opt.conf_thres,
                       opt.iou_thres, opt.classes, opt.agnostic_nms, model=model)
                strip_optimizer(opt.weights)
        else:
            detect(opt.source, opt.img_size, opt.device, opt.augment, opt.conf_thres,
                   opt.iou_thres, opt.classes, opt.agnostic_nms, model=model)
