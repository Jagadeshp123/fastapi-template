import matplotlib.pyplot as plt
import numpy as np
import os
from PIL import Image
from tqdm import tqdm


def main():
    # Update the following paths (dataset name, train/test, target)
    dataset_path = "../datasets/alcohol_dataset_extended"
    images_path = dataset_path + "/images" + "/train"
    annotations_path = dataset_path + "/labels" + "/train"
    target_path = dataset_path + "/images" + "/train_fsl"  # class-spec subfolders base path
    classes_txt_path = dataset_path + "/classes.txt"
    # Load classes.txt and create class-spec subfolders
    id_to_class_name = {}
    classes = 0
    if not os.path.exists(target_path):
        os.mkdir(target_path)
    with open(classes_txt_path, 'r') as class_names:
        for line_number, line in enumerate(class_names):
            id_to_class_name[line_number] = line.rstrip()
            classes += 1
            if not os.path.exists(target_path + "/" + line.rstrip()):
                os.mkdir(target_path + "/" + line.rstrip())

    for root, _, filenames in sorted(os.walk(images_path, followlinks=True)):
        for filename in tqdm(sorted(filenames)):
            img_path = os.path.join(root, filename)
            img = Image.open(img_path)
            single_annotation_file_path = os.path.join(annotations_path, filename)
            single_annotation_file_path = single_annotation_file_path[:-3] + "txt"
            try:
                with open(single_annotation_file_path, 'r') as single_annotation_file:
                    single_annotation_data = single_annotation_file.readlines()
            except FileNotFoundError:
                continue
            bounding_boxes = []
            labels = []
            for i, line in enumerate(single_annotation_data):
                label, x, y, w, h = map(float, line.split(' '))
                # get all labels in an image
                labels.append(label)
                bounding_boxes.append([label, x, y, w, h])

                # Crop bounding box and save
                img_x, img_y = img.size
                left = max(0, int(img_x * (x - w / 2)))
                right = min(int(img_x * (x + w / 2)), img_x)
                top = max(0, int(img_y * (y - h / 2)))
                bottom = min(int(img_y * (y + h / 2)), img_y)
                if left == right or top == bottom:
                    continue
                cropped_object = img.crop((left, top, right, bottom))
                # plt.imshow(cropped_object)
                cropped_img_filename = filename[:-4] + f"_{i}.png"
                try:
                    cropped_img_path = target_path + "/" + id_to_class_name[int(label)] + "/" + cropped_img_filename
                    if not os.path.exists(cropped_img_path):
                        cropped_object.save(cropped_img_path)
                except KeyError:
                    pass


if __name__ == '__main__':
    main()
