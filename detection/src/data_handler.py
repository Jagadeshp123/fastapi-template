import torch
import numpy as np
import collections
from PIL import Image
import albumentations as A
from operator import itemgetter
from torchvision import datasets
from torchvision import transforms
from torch.utils.data.dataset import Dataset
import torchvision.transforms.functional as f


class CustomDataset(Dataset):
    """
    Class for processing the input dataset and applying augmentation if required

    :param dataset: Input dataset to process
    :param to_augment: Optional; If True, apply various augmentations to data during the getitem() function call
    """
    def __init__(self, dataset, to_augment=False, original_image_size=256, use_random_center_crops=True):
        self.dataset = dataset
        self.to_augment = to_augment
        self.original_image_size = original_image_size
        self.use_random_center_crops = use_random_center_crops
        self.to_pil_image = transforms.ToPILImage()
        self.to_tensor = transforms.ToTensor()
        self.A_augmentation = A.Compose([
            A.RandomShadow(p=0.2),
            A.Blur(blur_limit=3, p=0.1),
        ])
        self.data_augmentation = [
            transforms.RandomRotation(degrees=(-90, 90)),
            transforms.RandomHorizontalFlip(p=0.5),
            transforms.RandomVerticalFlip(p=0.5),
            transforms.RandomApply([transforms.GaussianBlur(kernel_size=(5, 9), sigma=(0.1, 5))], p=0.3),
            transforms.RandomErasing(p=0.1, scale=(0.02, 0.1), ratio=(0.3, 3.3)),
            # transforms.RandomPerspective(distortion_scale=0.5, p=0.1),
            transforms.RandomApply(
                [transforms.RandomAffine(degrees=(30, 70), translate=(0.1, 0.3), scale=(0.5, 0.75))], p=0.1),
            transforms.RandomApply(
                [transforms.ColorJitter(brightness=0.5, contrast=0.5, hue=0.05)], p=0.3),
            transforms.Resize(size=[self.original_image_size, self.original_image_size])
            # transforms.RandomGrayscale(p=0.2)
        ]
        self.only_resize = transforms.Resize(size=[self.original_image_size, self.original_image_size])

        if self.use_random_center_crops:
            crop_list = []
            for i in range(0, 7, 1):
                multiplier = 1.0 - i / 10
                crop_list.append(
                    transforms.CenterCrop(size=(int(self.original_image_size * 3 * multiplier),
                                                int(self.original_image_size * 3 * multiplier)))
                )
            self.data_augmentation.insert(0, transforms.RandomChoice(crop_list))

        self.data_augmentation = transforms.Compose(self.data_augmentation)

        self.n_classes = len(self.dataset.classes)
        self.label_to_indices = collections.defaultdict(list)
        self.labels_set = np.array(set(dataset.class_to_idx.values()))

        self.instances_per_class = dict(collections.Counter(dataset.targets))
        self.idx_to_class_label = {v: k for k, v in self.dataset.class_to_idx.items()}

        # print("Class to ID", self.dataset.class_to_idx)
        # print("Number of instances per class", sorted(self.instances_per_class.items()))

        first_index = 0
        last_index = 0
        for label, num_instances in self.instances_per_class.items():
            last_index += num_instances
            self.label_to_indices[label].append(first_index)
            self.label_to_indices[label].append(last_index)
            first_index += num_instances

    def __getitem__(self, index):
        img, label = self.dataset[index][0], self.dataset[index][1]

        if self.to_augment:
            # PIL-NUMPY image conversions are needed for albumentations... later, this must be revised and optimized
            pil_img = self.to_pil_image(img)
            np_img = np.array(pil_img)
            np_img = self.A_augmentation(image=np_img)
            pil_img = Image.fromarray(np_img['image'])
            img = self.to_tensor(pil_img)
            return self.data_augmentation(img), label
        else:
            if self.use_random_center_crops:
                return self.only_resize(img), label
            else:
                return img, label

    def __len__(self):
        return len(self.dataset)


class CustomSubset(Dataset):
    """
    A custom class to handle random train-test sets separation
    by keeping the original dataset's attributes

    :param full_dataset: Input dataset to process
    :param indices: List of indices used for separating the input dataset
    """
    def __init__(self, full_dataset, indices):
        self.indices = indices
        self.dataset = torch.utils.data.Subset(full_dataset, indices)
        self.full_dataset = full_dataset
        self.classes = full_dataset.classes
        self.class_to_idx = full_dataset.class_to_idx
        self.targets = list(itemgetter(*indices)(full_dataset.targets))

    def __getitem__(self, idx):
        return self.full_dataset[self.indices[idx]]

    def __len__(self):
        return len(self.indices)


def load_dataset_from_folder(dataset_path, input_image_size, use_random_center_crops=True):
    """
    Load images into dataset based on folder structure

    :param dataset_path: Path to where the input images are
    :param input_image_size: Size of the input images to be resized to
    :param use_random_center_crops: Use random center crops during augmentation - influences resizing
    :return: Processed dataset (in the class structure that is specified by the folder structure) and number of classes
    """

    if use_random_center_crops:
        multiplier = 3
    else:
        multiplier = 1

    transform = transforms.Compose([
        SquarePad(),  # square padding for keeping original aspect ratio
        transforms.Resize(size=[input_image_size * multiplier, input_image_size * multiplier]),  # resize image
        transforms.ToTensor()  # transform to tensor and rescale to the range [0, 1]
    ])

    data = datasets.ImageFolder(dataset_path, transform=transform)
    class_number = len(data.classes)
    print(f"Number of classes in the dataset: {class_number}")
    print(f"Number of images in the dataset: {len(data)}")
    return data, class_number


class SquarePad(torch.nn.Module):
    """
    Apply square padding to image to keep its aspect ratio during resizing
    """

    def __init__(self):
        super().__init__()
        pass

    @staticmethod
    def forward(img):
        """
        Args:
            img (Tensor): Image to be padded.

        Returns:
            Tensor: Square padded image.
        """
        max_wh = max(img.size)
        p_left, p_top = [(max_wh - s) // 2 for s in img.size]
        p_right, p_bottom = [max_wh - (s + pad) for s, pad in zip(img.size, [p_left, p_top])]
        padding = (p_left, p_top, p_right, p_bottom)
        return f.pad(img, padding, 0, 'constant')

