import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import pywt
# from scipy.signal import butter, filtfilt
# from scipy.signal import find_peaks


fs = 130.0
nyq = 0.5 * fs


def show_descriptor_plot(coeff_sums_by_level, peaks, input_images):
    plt.rc('font', **{'size': 5})
    x = [x for x in range(len(input_images))]
    my_xticks = [x[1] for x in input_images]
    plt.xticks(x, my_xticks)
    plt.plot(coeff_sums_by_level[-1])
    plt.plot(peaks[-1], np.array(coeff_sums_by_level[-1])[peaks[-1]], "x")
    plt.xlabel("Images")
    plt.ylabel("Wavelet scores for sharpness")
    plt.show()


def find_n_least_blurry_imgs(image_folder_path, N, visualize=False):
    """
        :param image_folder_path: Folder to images
        :param N: number of least blurry images to return
        :param visualize: Show plot with sharpness scores
        :return: A list with the selected images
        """

    files = os.listdir(image_folder_path)

    imgs = []
    for img in files:
        if img.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')):
            img_path = os.path.join(image_folder_path, img)
            img_array = cv2.imread(img_path)
            imgs.append([img_array, img])

    coeffs_level = 4
    coeff_sums_by_level = [[] for _ in range(coeffs_level)]

    # calculate optical flow a wavelet transforms for the frames
    for frame_counter in range(0, len(imgs)):
        # Read the next frame
        frame = imgs[frame_counter][0]

        coeffs = pywt.wavedec2(frame, 'db5', level=coeffs_level)
        # sum wavelet transform coefficients by level for the frame and store it
        for i in range(0, coeffs_level):
            s = 0
            for j in range(len(coeffs[i])):
                s += np.sum(np.abs(coeffs[i][j]))
            coeff_sums_by_level[i].append(s)

    # coeff_sums_by_level = np.array(coeff_sums_by_level)
    all_max_peaks = []
    for i in range(0, coeffs_level):
        # find highest N indexes
        max_peaks = np.argsort(coeff_sums_by_level[i])[-min(N, len(imgs)):]
        # max_peaks, _ = find_peaks(coeff_sums_by_level[i])

        # # find_peaks() works by comparing neighboring values - so will not identify peaks that occur at the beginning
        # # or end of the array. One workaround is to use np.concatenate() to insert the minimum value of the array
        # # at the beginning and end, and then subtract 1 from the peaks variable
        # if max_peaks.shape[0] == 0:
        #     max_peaks, _ = find_peaks(np.concatenate(([min(coeff_sums_by_level[i])],
        #                                               coeff_sums_by_level[i],
        #                                               [min(coeff_sums_by_level[i])])))

        all_max_peaks.append(max_peaks)
    if visualize:
        show_descriptor_plot(coeff_sums_by_level, all_max_peaks, imgs)

    return [imgs[x][1] for x in all_max_peaks[-1]]

