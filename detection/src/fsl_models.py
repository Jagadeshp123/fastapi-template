import torch.nn as nn
import torch.nn.functional as f


class TripletNet(nn.Module):
    def __init__(self, embedding_net):
        super(TripletNet, self).__init__()
        self.embedding_net = embedding_net

    def forward(self, x):
        output = self.embedding_net(x)
        return output

    def get_embedding(self, x):
        return self.embedding_net(x)


class EmbeddingNet(nn.Module):
    def __init__(self, input_image_size=256, latent_vector_size=128):
        super(EmbeddingNet, self).__init__()
        self.convnet = nn.Sequential(nn.Conv2d(in_channels=3, out_channels=64, kernel_size=(3, 3),
                                               stride=(2, 2), padding=1),
                                     nn.InstanceNorm2d(64),
                                     nn.LeakyReLU(),
                                     nn.Conv2d(in_channels=64, out_channels=128, kernel_size=(3, 3),
                                               stride=(2, 2), padding=1),
                                     nn.InstanceNorm2d(128),
                                     nn.LeakyReLU(),
                                     nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(3, 3),
                                               stride=(2, 2), padding=1),
                                     nn.InstanceNorm2d(256),
                                     nn.LeakyReLU(),
                                     nn.Conv2d(in_channels=256, out_channels=512, kernel_size=(3, 3),
                                               stride=(2, 2), padding=1),
                                     nn.InstanceNorm2d(512),
                                     nn.LeakyReLU()
                                     )

        self.fc = nn.Sequential(nn.Linear(512 * input_image_size // (2 ** 4) * input_image_size // (2 ** 4), 512),
                                nn.LeakyReLU(),
                                nn.LayerNorm(512),
                                nn.Linear(512, latent_vector_size))


    def forward(self, x):
        output = self.convnet(x)
        output = output.view(output.size()[0], -1)
        output = self.fc(output)
        return output

    def get_embedding(self, x):
        return self.forward(x)
