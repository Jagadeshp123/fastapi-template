# Cartzy Object Detection

Use the below command to run the setup :

    ```
        python src/demo.py --algorithm combined_yolo_optical_flow --video_path "./test-videos/1.mp4" --yolo_model_path "./best.pt" --skip_inference

        ```

Please Consider the cropped .png images that are saved under "cropped_bbs/test-videos_1.mp4_0/combined_detections/high_iou_yolo_crops/"