from fastapi import APIRouter,Form
# from pydantic import BaseModel
from .inference import load_model, pre_process
import numpy as np
import pandas as pd
import uvicorn
import os

def rec_func(model: str = Form(...), images: str = Form(...), npy_folder_name: str = Form(...), labels_csv: str = Form(...)):
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-m', '--model', help="model path")      
    # parser.add_argument('-i', '--images', help="Folder of Images path")
    # parser.add_argument('-npy', '--npy_folder_name', help="Npy folder name, where all npy files will be saved")
    # parser.add_argument('-csv', '--labels_csv', help="csv label path, to match the output index with actual SKU")
    # args = parser.parse_args()
    model_path = model
    folder_path = images
    npy_folder = npy_folder_name
    csv = labels_csv
    os.makedirs(npy_folder, exist_ok=True) 
    #Load Model
    model, model_2 = load_model(model_path)
    
    labels_df = pd.read_csv(csv)
    
    try:
        for img_name in os.listdir(folder_path):
            img_path = os.path.join(folder_path,img_name)
            #Pre-Process Image
            image_tensor = pre_process(img_path)
            
            # Sending Image Tensor to model, to get softmax output
            softmax_score = model(image_tensor)
            
            # Sending Image Tensor to model, to get embedding of shape 1024
            embd = model_2(image_tensor)
            
            # Converting to numpy array 
            embds_np = np.array(embd)
            
            #Choosing the Index of the Maximum score in the Softmax output
            top_index = np.argmax(softmax_score[0].numpy())
            top_score = np.max(softmax_score[0].numpy())
            #mapping Index with SKU 
            label = labels_df[labels_df['label']==top_index].class_name.iloc[0]
            print("SKU ",label)
            
            # print(top_index)
            print("Confidence",top_score)
            name = img_name.split(".")[0]
            np.save(npy_folder+'/'+name+".npy",embds_np)
    except Exception as e:
        print(e)
    return "done"
 

# if __name__ == "__main__":
#     uvicorn.run(app, host="0.0.0.0", port=8000)