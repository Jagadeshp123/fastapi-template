import tensorflow as tf
import os 
import argparse
import numpy as np
import pandas as pd

class WarmUpCosine(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(
        self, learning_rate_base=0.03, total_steps=13000, warmup_learning_rate=0.006, warmup_steps=10
    ):
        super(WarmUpCosine, self).__init__()

        self.learning_rate_base = learning_rate_base
        self.total_steps = total_steps
        self.warmup_learning_rate = warmup_learning_rate
        self.warmup_steps = warmup_steps
        self.pi = tf.constant(np.pi)

    def __call__(self, step):
        if self.total_steps < self.warmup_steps:
            raise ValueError("Total_steps must be larger or equal to warmup_steps.")
        learning_rate = (
            0.5
            * self.learning_rate_base
            * (
                1
                + tf.cos(
                    self.pi
                    * (tf.cast(step, tf.float32) - self.warmup_steps)
                    / float(self.total_steps - self.warmup_steps)
                )
            )
        )

        if self.warmup_steps > 0:
            if self.learning_rate_base < self.warmup_learning_rate:
                raise ValueError(
                    "Learning_rate_base must be larger or equal to "
                    "warmup_learning_rate."
                )
            slope = (
                self.learning_rate_base - self.warmup_learning_rate
            ) / self.warmup_steps
            warmup_rate = slope * tf.cast(step, tf.float32) + self.warmup_learning_rate
            learning_rate = tf.where(
                step < self.warmup_steps, warmup_rate, learning_rate
            )
        return tf.where(
            step > self.total_steps, 0.0, learning_rate, name="learning_rate"
        )
    def get_config(self):
        config = {
            'warmup_steps': self.warmup_steps,
            'total_steps': self.total_steps}
        return config

def pre_process(file):
    "Preprocessing the Image"
    image = tf.io.read_file(file)
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.image.resize(image, [224, 224])
    image = tf.expand_dims(image,0)

    return image

def load_model(model_name):
    '''
    Loads Keras Model: Model with Softmax Layer
    Creates a Embedding Model: Outputs an Embedding of Shape (1024)
    '''
    model = tf.keras.models.load_model(model_name, custom_objects={"WarmUpCosine": WarmUpCosine})
    #Embedding Model , Removing Softmax Layer, 
    model_2 = tf.keras.Model(inputs=model.input, outputs=model.get_layer('keras_layer').output)
    return model, model_2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model', help="model path")      
    parser.add_argument('-i', '--images', help="Folder of Images path")
    parser.add_argument('-npy', '--npy_folder_name', help="Npy folder name, where all npy files will be saved")
    parser.add_argument('-csv', '--labels_csv', help="csv label path, to match the output index with actual SKU")
    args = parser.parse_args()
    model_path = args.model
    folder_path = args.images
    npy_folder = args.npy_folder_name
    csv = args.labels_csv
    os.makedirs(npy_folder, exist_ok=True) 
    #Load Model
    model, model_2 = load_model(model_path)
    
    labels_df = pd.read_csv(csv)
    
    for img_name in os.listdir(folder_path):
        img_path = os.path.join(folder_path,img_name)
        #Pre-Process Image
        image_tensor = pre_process(img_path)
        
        # Sending Image Tensor to model, to get softmax output
        softmax_score = model(image_tensor)
        
        # Sending Image Tensor to model, to get embedding of shape 1024
        embd = model_2(image_tensor)
        
        # Converting to numpy array 
        embds_np = np.array(embd)
        
        #Choosing the Index of the Maximum score in the Softmax output
        top_index = np.argmax(softmax_score[0].numpy())
        top_score = np.max(softmax_score[0].numpy())
        #mapping Index with SKU 
        label = labels_df[labels_df['label']==top_index].class_name.iloc[0]
        print("SKU ",label)
        
        # print(top_index)
        print("Confidence",top_score)
        name = img_name.split(".")[0]
        np.save(npy_folder+'/'+name+".npy",embds_np)