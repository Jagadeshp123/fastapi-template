**Inference Setup for Recognition**

**Installing Requirements**
```
conda create -n cartzy python=3.9

conda activate cartzy

pip3 install -r requirement.txt

conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
```
**Download the Model from Cloud**

`gsutil cp -r gs://smart-cart-storage/cartzy_freshpik_vitl16_v1_798_skus_bs32_epoch1.zip .`



**Command to run the script**

`python3 inference.py -m=cartzy_freshpik_vitl16_v1_798_skus_bs32_01-0.036628/ -i=test_samples/ -npy=npy_folder -csv=v1_freshpik_798__classes_labels.csv`


```
m = Model Path 
i = Test Image npy_folder
npy-folder = Embeddings of the models on the test images will be saved in this folder
csv = Mapping the csv to the SKU's
```




**To Resolve Graph execution Error**

```
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/
$ mkdir /home/User/miniconda3/envs/cartzy/bin/nvvm/libdevice
$ export XLA_FLAGS=--xla_gpu_cuda_data_dir=/home/User/miniconda3/envs/cartzy/bin/
```

