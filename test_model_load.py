import torch
from fastapi import FastAPI,Form
import uvicorn
from typing import Optional
import torch.nn as nn

model = torch.load("/home/user/Downloads/resnet18-5c106cde.pth")

model.eval()
# app = FastAPI()

# class TheModelClass(nn.Module):
#     def __init__(self):
#         super(TheModelClass, self).__init__()
#         self.conv1 = nn.Conv2d(3, 6, 5)
#         self.pool = nn.MaxPool2d(2, 2)
#         self.conv2 = nn.Conv2d(6, 16, 5)
#         self.fc1 = nn.Linear(16 * 5 * 5, 120)
#         self.fc2 = nn.Linear(120, 84)
#         self.fc3 = nn.Linear(84, 10)

#     # def forward(self, x):
#     #     x = self.pool(F.relu(self.conv1(x)))
#     #     x = self.pool(F.relu(self.conv2(x)))
#     #     x = x.view(-1, 16 * 5 * 5)
#     #     x = F.relu(self.fc1(x))
#     #     x = F.relu(self.fc2(x))
#     #     x = self.fc3(x)
#     #     return x

# @app.post("/test_model_load")
# def model_loading(w:str = Form()):
#     print(w)

#     ckpt = torch.load(w)
#     return "done"

# if __name__ == "__main__":
#     uvicorn.run(app, host="0.0.0.0", port=8000)